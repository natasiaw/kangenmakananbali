<?php

  return [
    'error403' => 'You don\'t have permission to access the resource.',
    'datepicker_date_format' => 'd/m/Y',
  ];
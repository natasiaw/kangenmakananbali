<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

use App\Models\Master\Category;

class Transactions extends Model
{
    protected $table = 'transactions';
    
    protected $fillable = [
        'id_m_category',
        'date',
        'name',
        'amount',
        'note',
        'status'
    ];

    public function category() {
        return $this->belongsTo(Category::class, 'id_m_category', 'id' )->select('id', 'name', 'id_m_category_type');
    }
}

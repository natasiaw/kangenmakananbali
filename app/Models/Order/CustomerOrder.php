<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

use App\Models\Master\Customer;
use App\Models\Master\OrderStatus;
use App\Models\Master\Courier;
use App\Models\Master\Promo;

use App\Models\Order\OrderDetail;

class CustomerOrder extends Model
{
    protected $table = 'customer_order';
    
    protected $fillable = [
        'm_customer_id',
        'm_courier_id',
        'm_promo_id',
        'm_order_status_id',
        'order_date',
        'shipping_cost',
        'weight',
        'plan_shipping_date',
        'actual_shipping_date',
        'tracking_number',
        'note',
        'status'
    ];

    public function customer() {
        return $this->belongsTo(Customer::class, 'm_customer_id', 'id' );
    }

    public function order_status() {
        return $this->belongsTo(OrderStatus::class, 'm_order_status_id', 'id' );
    }

    public function order_detail() {
        return $this->hasMany(OrderDetail::class, 'id_customer_order', 'id');
    }

    public function courier() {
        return $this->belongsTo(Courier::class, 'm_courier_id', 'id' );
    }

    public function promo() {
        return $this->belongsTo(Promo::class, 'm_promo_id', 'id' );
    }
}

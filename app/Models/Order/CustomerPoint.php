<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

use App\Models\Master\Customer;

class CustomerPoint extends Model
{
    protected $table = 'customer_point';

    protected $fillable = [
        'id_m_customer',
        'point',
        'note',
        'status'
    ];

    public function customer() {
        return $this->belongsTo(Customer::class, 'id_m_customer', 'id' );
    }
}

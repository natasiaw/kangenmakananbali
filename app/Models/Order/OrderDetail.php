<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;
use App\Models\Master\Menu;

class OrderDetail extends Model
{
    protected $table = 'order_detail';
    
    protected $fillable = [
        'id_customer_order',
        'm_menu_id',
        'original_price',
        'online_price',
        'kmb_price',
        'packing_fee',
        'quantity',
        'note',
        'status'
    ];

    public function menu() {
        return $this->belongsTo(Menu::class, 'm_menu_id', 'id' )->select('id', 'name', 'm_restaurant_id');
    }
}


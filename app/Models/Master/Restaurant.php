<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $table = 'm_restaurant';
    
    protected $fillable = [
        'name',
        'address',
        'id_m_postal_code',
        'phone',
        'note',
        'status'
    ];

    public function postal_code() {
        return $this->belongsTo(PostalCode::class, 'id_m_postal_code', 'id' )->select('id', 'village', 'district', 'regency', 'postal_code', 'id_m_province');
    }
}

<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    
    protected $table = 'm_country';
    
    protected $fillable = [
        'name',
        'note',
        'status'
    ];
}

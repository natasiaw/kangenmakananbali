<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    protected $table = 'm_courier';
    
    protected $fillable = [
        'name',
        'note',
        'status'
    ];
}

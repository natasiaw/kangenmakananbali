<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class PostalCode extends Model
{
    protected $table = 'm_postal_code';
    
    protected $fillable = [
        'id',
        'village',
        'district',
        'regency',
        'id_m_province',
        'postal_code',
        'note',
        'status'
    ];

    public function province() {
        return $this->belongsTo(Province::class, 'id_m_province' )->select('id', 'name');
    }
}

<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'm_category';
    
    protected $fillable = [
        'id_m_category_type',
        'name',
        'note',
        'status'
    ];

    public function category_type() {
        return $this->belongsTo(CategoryType::class, 'id_m_category_type', 'id' )->select('id', 'name', 'is_expenses');
    }
}

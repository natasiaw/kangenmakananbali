<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    protected $table = 'm_promo';
    
    protected $fillable = [
        'name',
        'price',
        'note',
        'status'
    ];
}

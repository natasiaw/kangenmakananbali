<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class CategoryType extends Model
{
    protected $table = 'm_category_type';
    
    protected $fillable = [
        'name',
        'note',
        'status',
        'is_expenses'
    ];
}

<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use App\Models\Order\CustomerPoint;

class Customer extends Model
{
    protected $table = 'm_customer';
    
    protected $fillable = [
        'name',
        'address',
        'id_m_postal_code',
        'phone',
        'note',
        'status'
    ];

    public function postal_code() {
        return $this->belongsTo(PostalCode::class, 'id_m_postal_code', 'id' )->select('id', 'village', 'district', 'regency', 'postal_code', 'id_m_province');
    }

    public function point() {
        return $this->hasMany(CustomerPoint::class, 'id_m_customer', 'id', 'status')->where('status',1);
    }
}

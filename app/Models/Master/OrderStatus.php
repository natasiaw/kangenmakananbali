<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $table = 'm_order_status';
    
    protected $fillable = [
        'name',
        'note',
        'status'
    ];
}

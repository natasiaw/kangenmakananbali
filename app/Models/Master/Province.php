<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'm_province';
    
    protected $fillable = [
        'id_m_country',
        'name',
        'note',
        'status'
    ];

    public function country() {
        return $this->belongsTo(Country::class, 'id_m_country')->select('id', 'name');
    }
}

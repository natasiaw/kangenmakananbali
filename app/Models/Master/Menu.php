<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'm_menu';
    
    protected $fillable = [
        'm_restaurant_id',
        'name',
        'original_price',
        'kmb_price',
        'po_status',
        'note',
        'status'
    ];

    public function restaurant() {
        return $this->belongsTo(Restaurant::class, 'm_restaurant_id', 'id' )->select('id', 'name');
    }
}

<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master\Customer;
use App\Models\Order\CustomerPoint;


use Carbon\Carbon;

class CustomerPointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'order.customer_point.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customerPoint = new CustomerPoint();
        $customer       = Customer::where('status', 1)->get();

        return view( 
            'order.customer_point.form', 
            [
                'customerPoint' => $customerPoint,
                'customer'      => $customer,
            ] 
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'point' => ['required']
        ]);

        $model = CustomerPoint::create(
            [
                'id_m_customer' => $request->customer,
                'point'         => $request->point,
                'note'          => $request->note, 
                'status'        => (isset( $request->status ) ? 1 : 0)
            ]
        );
        
        return redirect( '/customerPoint' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order\CustomerPoint  $customerPoint
     * @return \Illuminate\Http\Response
     */
    public function show(CustomerPoint $customerPoint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order\CustomerPoint  $customerPoint
     * @return \Illuminate\Http\Response
     */
    public function edit(CustomerPoint $customerPoint)
    {
        $customerPoint = new CustomerPoint();
        $customer       = Customer::where('status', 1)->get();

        return view( 
            'order.customer_point.form', 
            [
                'customerPoint' => $customerPoint,
                'customer'      => $customer,
            ] 
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order\CustomerPoint  $customerPoint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerPoint $customerPoint)
    {
        request()->validate([
            'point' => ['required']
        ]);

        $model = CustomerPoint::find($customerPoint->id);  

        if ($model) {

            $model->id_m_customer   = $request->customer;
            $model->point           = $request->point;
            $model->note            = $request->note; 
            $model->status          = 1;

            $model->save();     

        }
        
        return redirect( route('customerPoint.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order\CustomerPoint  $customerPoint
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, CustomerPoint $customerPoint)
    {
        $model = CustomerPoint::find($customerPoint->id);

        if ($model) {
                
            $model->status      = 0;
            $model->deleted_at  = Carbon::now();
            $model->deleted_by  = 'Natasia';  

            $model->save();  

        }       

        return response()->json(true);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data()
    {
        $models =   CustomerPoint::where([
                        ['status', 1],
                    ])
                    ->with('customer.postal_code.province')
                    ->get();

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }
}

<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Order\CustomerOrder;
use App\Models\Order\OrderDetail;
use App\Models\Master\Customer;
use App\Models\Master\Courier;
use App\Models\Master\OrderStatus;
use App\Models\Master\Menu;
use App\Models\Master\Promo;
use App\Models\Finance\Transactions;
use App\Models\Order\CustomerPoint;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderStatus    = OrderStatus::where('status', 1)->get();
        return view( 'order.customer_order.index', compact('orderStatus') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customerOrder  = new CustomerOrder();
        $customer       = Customer::where('status', 1)->get();
        $courier        = Courier::where('status', 1)->get();
        $orderStatus    = OrderStatus::where('status', 1)->get();
        $promo          = Promo::where('status', 1)->get();
        $orderDetail    = array();

        return view( 
            'order.customer_order.form_edit', 
            [
                'model'         => $customerOrder,
                'customer'      => $customer,
                'courier'       => $courier,
                'orderStatus'   => $orderStatus,
                'promo'         => $promo,
                'orderDetail'   => $orderDetail,
            ] 
        );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = CustomerOrder::create(
            [
                'm_customer_id'         => $request->customer, 
                'm_courier_id'          => $request->courier, 
                'm_order_status_id'     => $request->order_status, 
                'order_date'            => Carbon::createFromFormat( config('global.datepicker_date_format'), $request->order_date )->timestamp,
                'shipping_cost'         => $request->shipping_cost, 
                'm_promo_id'            => $request->promo, 
                'additional_cost'       => $request->additional_cost, 
                //'plan_shipping_date'    => Carbon::createFromFormat( config('global.datepicker_date_format'), $request->plan_shipping_date )->timestamp,
                'weight'                => $request->weight, 
                'actual_shipping_date'  => Carbon::createFromFormat( config('global.datepicker_date_format'), $request->actual_shipping_date )->timestamp,
                'tracking_number'       => $request->tracking_number, 
                'note'                  => $request->note, 
                'status'                => (isset( $request->status ) ? 1 : 0)
            ]
        );

        
        
        for ( $i = 0; $i < count( $request->menu ); $i++ )
        {           
            if ( empty( $request->quantity[$i] ) ) {
                continue;
            }

            $kmbPrice = Menu::find( $request->menu[$i] );


            $orderDetail = OrderDetail::create([
                'id_customer_order' => $model->id,
                'm_menu_id'         => $request->menu[$i],
                'quantity'          => $request->quantity[$i], 
                'original_price'    => $kmbPrice->original_price, 
                'kmb_price'         => $kmbPrice->kmb_price, 
                'note'              => $request->note_detail[$i], 
                'status'            => 1
            ]);
           
        }

        return redirect( '/order' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order\CustomerOrder  $customerOrder
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view( 'order.customer_order.invoice', compact('id') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order\CustomerOrder  $customerOrder
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customerOrder  = CustomerOrder::find($id);  
        $customer       = Customer::where('status', 1)->get();
        $courier        = Courier::where('status', 1)->get();
        $orderStatus    = OrderStatus::where('status', 1)->get();
        $menu           = Menu::where('status', 1)->get();
        $promo          = Promo::where('status', 1)->get();
        $orderDetail    = OrderDetail::where([
                                ['id_customer_order', $id],
                                ['status', 1]
                            ])->get();

        return view( 
            'order.customer_order.form_edit', 
            [
                'model'         => $customerOrder,
                'customer'      => $customer,
                'courier'       => $courier,
                'orderStatus'   => $orderStatus,
                'menu'          => $menu,
                'orderDetail'   => $orderDetail,
                'promo'         => $promo
            ] 
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order\CustomerOrder  $customerOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CustomerOrder $customerOrder)
    {
        $model = CustomerOrder::find($request->id);  
        if ($model) {
                
            $model->m_courier_id            = $request->courier;
            $model->m_order_status_id       = $request->order_status; 
            $model->shipping_cost           = $request->shipping_cost; 
            $model->weight                  = $request->weight; 
            $model->actual_shipping_date    = Carbon::createFromFormat( config('global.datepicker_date_format'), $request->actual_shipping_date )->timestamp; 
            $model->tracking_number         = $request->tracking_number; 
            $model->note                    = $request->note; 
            //$model->plan_shipping_date      = Carbon::createFromFormat( config('global.datepicker_date_format'), $request->plan_shipping_date )->timestamp; 
            $model->m_promo_id               = $request->promo; 
            $model->additional_cost          = $request->additional_cost; 
            
            $model->save();     

            $total_transaction = $request->shipping_cost * $request->weight ;

            //if no status re-create order
            if($request->order_status == 0){
                $detail = OrderDetail::where('id_customer_order', $model->id)->delete();

                for ( $i = 0; $i < count( $request->menu ); $i++ )
                {           
                    if ( empty( $request->quantity[$i] ) ) {
                        continue;
                    }

                    $kmbPrice = Menu::find( $request->menu[$i] );


                    $orderDetail = OrderDetail::create([
                        'id_customer_order' => $model->id,
                        'm_menu_id'         => $request->menu[$i],
                        'quantity'          => $request->quantity[$i], 
                        'original_price'    => $kmbPrice->original_price, 
                        'kmb_price'         => $kmbPrice->kmb_price, 
                        'status'            => 1,
                        'note'              => $request->note_detail[$i], 
                    ]);
                
                }
            }

            $customer   = Customer::find( $model->m_customer_id );
            $promo      = Promo::find( $model->m_promo_id );

            //if paid, add to transaction
            if($request->order_status == 1){
                $orderDetail = OrderDetail::where('id_customer_order', $model->id)->get();
                
                foreach ($orderDetail as $o) {
                    $total_transaction = $total_transaction + ($o->quantity*$o->kmb_price);
                }

                $total_transaction -=$promo->price;

                $transactions = Transactions::create([
                    'id_m_category' => 8,
                    'date'          => Carbon::now()->timestamp,
                    'name'          => $customer->name,
                    'amount'        => $total_transaction,
                ]);

            }

            //if complete add to point
            $total_point = 0;
            if($request->order_status == 3){
                $orderDetail = OrderDetail::where('id_customer_order', $model->id)->get();
                
                foreach ($orderDetail as $o) {
                    $total_point = $total_point + ($o->quantity*$o->kmb_price)/1000;
                }

                $point = CustomerPoint::create(
                    [
                        'id_m_customer' => $model->m_customer_id,
                        'point'         => $total_point,
                        'note'          => "Order ". $request->actual_shipping_date, 
                        'status'        => 1
                    ]
                );
            }

        }

        return redirect( route('order.index') );

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order\CustomerOrder  $customerOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, CustomerOrder $customerOrder)
    {
        $model = customerOrder::find($customerOrder->id);

        if ($model) {
            $this->old_data = $this->serialize_data($model);
                
            $model->status      = 0;
            $model->deleted_at  = Carbon::now();
            $model->deleted_by  = auth()->user()->username;

            $model->save();  

            $this->write_log( __CLASS__, $model->id, $request->fullUrl(), __FUNCTION__, $request->ip(), $this->old_data, "" );
        }       

        return response()->json(true);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data(Request $request)
    {
        $order_status = $request->order_status;

        $models =   CustomerOrder::where([
                        ['status', 1],
                    ])
                    ->with([
                        'customer.postal_code.province',
                        'order_status',
                        'order_detail.menu.restaurant',
                        'promo'
                        ])
                    ->orderBy('m_order_status_id', 'asc');

                    if ($order_status != -1) {
                        $models->where('m_order_status_id', $order_status);
                      }else{
                        $models->where('m_order_status_id', '<>', 3);
                      }

        $total_record = $models->count();

        $offset = 0;
        $perPage = 50;
        if ($request->has("start")) {
            $start      = $request->input("start");
            $perPage    = $request->input("length");
            $offset     = (($start / $perPage)) * $perPage;

            if ($offset < 0) {
                $offset = 0;
            }
        } 
        $models->skip($offset)->take($perPage);

        $data = [
            'recordsTotal'      => $total_record,
            'recordsFiltered'   => $total_record,
            'per_page'          => $request->has('length') ? $request->input('length') : 50,
            'current_page'      => $request->has('start') ? $request->input('start') : 1,
            'data'              => $models->get()
        ];


        return response()->json($data);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data_paid(Request $request)
    {
        
        $models =   CustomerOrder::where([
                        ['status', 1],
                        ['m_order_status_id', 1]
                    ])
                    ->orWhere([
                        ['m_order_status_id', 10],
                    ])
                    ->with([
                        'customer.postal_code.province',
                        'order_status',
                        'order_detail.menu.restaurant',
                        'courier'
                        ]);

/*
        $models =   CustomerOrder::where([
            ['status', 1]
        ])
        ->with([
            'customer.postal_code.province',
            'order_status',
            'order_detail.menu.restaurant',
            'courier'
            ]);

            */
        $total_record = $models->count();

        $offset = 0;
        $perPage = 50;
        if ($request->has("start")) {
            $start      = $request->input("start");
            $perPage    = $request->input("length");
            $offset     = (($start / $perPage)) * $perPage;

            if ($offset < 0) {
                $offset = 0;
            }
        } 
        $models->skip($offset)->take($perPage);

        $data = [
            'recordsTotal'      => $total_record,
            'recordsFiltered'   => $total_record,
            'per_page'          => $request->has('length') ? $request->input('length') : 50,
            'current_page'      => $request->has('start') ? $request->input('start') : 1,
            'data'              => $models->get()
        ];


        return response()->json($data);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_invoice(Request $request)
    {
        $models =   CustomerOrder::where([
                        ['id', $request->id],
                        ['status', 1],
                    ])
                    ->with([
                        'customer.postal_code.province',
                        'order_status',
                        'order_detail.menu.restaurant',
                        'courier',
                        'promo'
                        ])
                    ->get();

        return response()->json($models);
    }
}

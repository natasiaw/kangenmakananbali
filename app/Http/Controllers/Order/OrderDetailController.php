<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Models\Order\OrderDetail;
use Illuminate\Http\Request;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'quantity' => ['required']
        ]);

        $model = OrderDetail::create([
            'id_customer_order' => $request->customer_order_id,
            'm_menu_id'         => $request->menu,
            'original_price'    => $request->original_price, 
            'online_price'      => $request->online_price, 
            'packing_fee'       => $request->packing_fee, 
            'quantity'          => $request->quantity, 
            'status'            => 1
        ]);

        return response()->json(['success'=>'']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function show(OrderDetail $orderDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderDetail $orderDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderDetail $orderDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order\OrderDetail  $orderDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderDetail $orderDetail)
    {
        //
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @param  int  $id_m_regency
     * @return \Illuminate\Http\Response
     */
    public function get_data_by_customer_order_id(Request $request)
    {

        $models = OrderDetail::where(['status' => 1, 'id_customer_order' => $request->id])->with('menu')->get();
            
        return response()->json($models);
    }
}

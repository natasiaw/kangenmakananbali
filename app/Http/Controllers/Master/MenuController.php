<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Menu;
use App\Models\Master\Restaurant;
use Illuminate\Http\Request;
use Carbon\Carbon;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'master.menu.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu       = new Menu();
        $restaurant = Restaurant::select('id', 'name')->where('status', 1)->get();

        return view( 
            'master.menu.form', 
            [
                'model'         => $menu, 
                'restaurant'    => $restaurant,
            ] 
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Menu::create(
            [
                'm_restaurant_id'   => $request->restaurant, 
                'name'              => $request->name, 
                'original_price'    => $request->price_original, 
                'online_price'      => $request->online_price, 
                'kmb_price'         => $request->kmb_price, 
                'note'              => $request->note, 
                'status'            => (isset( $request->status ) ? 1 : 0),
                'po_status'         => (isset( $request->po_status ) ? 1 : 0)
            ]
        );
        
        return redirect( '/menu' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        $restaurant = Restaurant::select('id', 'name')->where('status', 1)->get();

        return view( 
            'master.menu.form', 
            [
                'model'         => $menu, 
                'restaurant'    => $restaurant,
            ] 
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Menu::find($menu->id);  

        if ($model) {
                
            $model->original_price      = $request->price_original;
            $model->kmb_price           = $request->kmb_price; 
            $model->online_price        = $request->online_price; 
            $model->name                = $request->name; 
            $model->note                = $request->note; 
            $model->status              = (isset( $request->status ) ? 1 : 0);
            $model->po_status           = (isset( $request->po_status ) ? 1 : 0);

            $model->save();     

        }
        
        return redirect( route('menu.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Menu $menu)
    {

        $model = Menu::find($menu->id);

        if ($model) {
                
            $model->status      = 0;
            $model->deleted_at  = Carbon::now();
            $model->deleted_by  = "Natasia";  

            $model->save();  

        }       

        return response()->json(true);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data()
    {
        $models =   Menu::where([
                        ['status', 1],
                    ])
                    ->with([
                        'restaurant'
                        ])
                    ->get();

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data_for_select(Request $request)
    {
        if($request->ajax()){
            $menus =    Menu::where([
                            ['status', 1]
                        ]) 
                        ->where(function($q) use ($request) {
                            $q->where('name', 'like', '%' . $request->search . '%')
                            ->orWhereHas('restaurant', function($q) use ($request) {
                                $q->where('name', 'like', '%' . $request->search . '%');
                            });
                        })
                        ->with('restaurant')
                        ->get();
            
            $result = array();
            $key = 0;
            foreach ($menus as $menu) {
                $result[$key]['id']     = $menu->id;
                $result[$key]['text']   = $menu->restaurant->name ." - ". $menu->name;
                $key++;
            }

            return response()->json($result);
        }
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @param  int  $id_m_country
     * @return \Illuminate\Http\Response
     */
    public function get_data_by_id(Request $request)
    {
        if($request->ajax()){
            $models = Menu::where(['status' => 1, 'id' => $request->parent])->get();
            
            return response()->json($models);
        }        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_release()
    {
        $models       = Menu::where([
                            ['status', 1],
                            ['po_status', 1]
                        ])
                        ->with([
                            'restaurant'
                            ])
                        ->orderBy('m_restaurant_id', 'asc')
                        ->get();
    
        $data = [
            'data' => $models
        ];

        return response()->json($data);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_halal()
    {
        $models       = Menu::where([
                            ['status', 1],
                            ['po_status', 1],
                            ['is_halal', 1]
                        ])
                        ->with([
                            'restaurant'
                            ])
                        ->orderBy('m_restaurant_id', 'asc')
                        ->get();
    
        $data = [
            'data' => $models
        ];

        return response()->json($data);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function idx_release()
    {
        return view( 'master.menu.release' );

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function idx_halal()
    {
        return view( 'master.menu.release_halal' );

    }
}

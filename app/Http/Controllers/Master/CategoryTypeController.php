<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\CategoryType;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CategoryTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'master.category_type.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categoryType = new CategoryType();
        return view( 'master.category_type.form', compact('categoryType') );
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = CategoryType::create(
            [
                'name'          => $request->name,
                'note'          => $request->note, 
                'is_expenses'   => $request->is_expenses, 
                'status' => (isset( $request->status ) ? 1 : 0)
            ]
        );
        
        return redirect( '/categoryType' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\CategoryType  $categoryType
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryType $categoryType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\CategoryType  $categoryType
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryType $categoryType)
    {
        return view( 'master.category_type.form', compact('categoryType') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\CategoryType  $categoryType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoryType $categoryType)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = CategoryType::find($categoryType->id);  

        if ($model) {
                
            $model->name        = $request->name;
            $model->note        = $request->note; 
            $model->is_expenses = (isset( $request->is_expenses ) ? 1 : 0); 
            
            $model->status      = 1;

            $model->save();     

        }
        
        return redirect( route('categoryType.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\CategoryType  $categoryType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, CategoryType $categoryType)
    {
        $model = CategoryType::find($categoryType->id);

        if ($model) {
                
            $model->status      = 0;
            $model->deleted_at  = Carbon::now();
            $model->deleted_by  = "Natasia";  

            $model->save();  

        }       

        return response()->json(true);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data()
    {
        $models =   CategoryType::select('id', 'name', 'note', 'is_expenses')
                    ->where([
                        ['status', 1],
                    ])
                    ->get();

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }
}

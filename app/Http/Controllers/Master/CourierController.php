<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Courier;
use Illuminate\Http\Request;

class CourierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'master.courier.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courier = new Courier();
        return view( 'master.courier.form', compact('courier') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Courier::create(
            [
                'name' => $request->name,
                'note' => $request->note, 
                'status' => (isset( $request->status ) ? 1 : 0)
            ]
        );
        
        return redirect( '/courier' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\Courier  $courier
     * @return \Illuminate\Http\Response
     */
    public function show(Courier $courier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Courier  $courier
     * @return \Illuminate\Http\Response
     */
    public function edit(Courier $courier)
    {
        return view( 'master.courier.form', compact('courier') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Courier  $courier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Courier $courier)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Courier::find($courier->id);  

        if ($model) {
            $this->old_data = $this->serialize_data($model);
                
            $model->name        = $request->name;
            $model->note        = $request->note; 
            $model->status      = 1;

            $model->save();     

        }
        
        return redirect( route('courier.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Courier  $courier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Courier $courier)
    {
        $model = Country::find($country->id);

        if ($model) {
            $this->old_data = $this->serialize_data($model);
                
            $model->status      = 0;
            $model->deleted_at  = Carbon::now();
            $model->deleted_by  = auth()->user()->username;  

            $model->save();  

        }       

        return response()->json(true);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data()
    {
        $models =   Courier::select('id', 'name', 'note')
                    ->where([
                        ['id', '!=', 0],
                        ['status', 1],
                    ])
                    ->get();

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }
}

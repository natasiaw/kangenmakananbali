<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Promo;
use Illuminate\Http\Request;

use Carbon\Carbon;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'master.promo.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $promo   = new Promo();

        return view( 
            'master.promo.form', 
            [
                'promo' => $promo, 
            ] 
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => ['required']
        ]);

        $model = Promo::create(
            [
                'name'                  => $request->name,
                'price'                 => $request->price,
                'note'                  => $request->note, 
                'status'                => (isset( $request->status ) ? 1 : 0)
            ]
        );
        
        return redirect( '/promo' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function show(Promo $promo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function edit(Promo $promo)
    {
        return view( 'master.promo.form', compact('promo') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promo $promo)
    {
        request()->validate([
            'name' => ['required']
        ]);

        $model = Promo::find($promo->id);  

        if ($model) {
            $this->old_data = $this->serialize_data($model);

            $model->name    = $request->name;
            $model->price   = $request->price;
            $model->note    = $request->note; 
            $model->status  = 1;

            $model->save();     

        }
        
        return redirect( route('promo.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Promo  $promo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Promo $promo)
    {
        $model = Promo::find($promo->id);

        if ($model) {
            $this->old_data = $this->serialize_data($model);
                
            $model->status      = 0;
            $model->deleted_at  = Carbon::now();
            $model->deleted_by  = "Natasia";  

            $model->save();  

        }       

        return response()->json(true);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data()
    {
        $models =   Promo::where([
                        ['status', 1],
                    ])
                    ->get();

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }
}

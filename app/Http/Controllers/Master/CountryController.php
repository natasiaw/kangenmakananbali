<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Master\Country;

class CountryController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'master.country.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        

        $country = new Country();
        return view( 'master.country.form', compact('country') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Country::updateOrCreate(
            ['name' => $request->name],
            [
                'note' => $request->note, 
                'status' => (isset( $request->status ) ? 1 : 0)
            ]
        );
        
        return redirect( '/country' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {

        return view( 'master.country.form', compact('country') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {

        return view( 'master.country.form', compact('country') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {

        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Country::find($country->id);  

        if ($model) {
            $this->old_data = $this->serialize_data($model);
                
            $model->name        = $request->name;
            $model->note        = $request->note; 
            $model->status      = 1;

            $model->save();     

            $this->new_data = $this->serialize_data($model);
            $this->write_log( __CLASS__, $model->id, $request->fullUrl(), __FUNCTION__, $request->ip(), $this->old_data, $this->new_data );
        }
        
        return redirect( route('country.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Country $country)
    {

        $model = Country::find($country->id);

        if ($model) {
            $this->old_data = $this->serialize_data($model);
                
            $model->status      = 0;
            $model->deleted_at  = Carbon::now();
            $model->deleted_by  = auth()->user()->username;  

            $model->save();  

            $this->write_log( __CLASS__, $model->id, $request->fullUrl(), __FUNCTION__, $request->ip(), $this->old_data, "" );
        }       

        return response()->json(true);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data()
    {
        $models =   Country::select('id', 'name', 'note')
                    ->where([
                        ['id', '!=', 0],
                        ['status', 1],
                    ])
                    ->get();

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }
}

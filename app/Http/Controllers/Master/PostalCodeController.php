<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Master\PostalCode;
use App\Models\Master\Province;

class PostalCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinces = Province::where('status', 1)->get();

        return view( 'master.postal_code.index', compact('provinces') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return abort(403);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\PostalCode  $regency
     * @return \Illuminate\Http\Response
     */
    public function show(PostalCode $regency)
    {
        return abort(403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\PostalCode  $regency
     * @return \Illuminate\Http\Response
     */
    public function edit(PostalCode $regency)
    {
        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\PostalCode  $regency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostalCode $regency)
    {
        return abort(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\PostalCode  $regency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, PostalCode $postalCode)
    {
        $model = PostalCode::find($postalCode->id);

        if ($model) {
            $this->old_data = $this->serialize_data($model);
                
            $model->status      = 0;
            $model->deleted_at  = Carbon::now();
            $model->deleted_by  = auth()->user()->username;

            $model->save();  

            $this->write_log( __CLASS__, $model->id, $request->fullUrl(), __FUNCTION__, $request->ip(), $this->old_data, "" );
        }       

        return response()->json(true);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data(Request $request)
    {
        $models =   PostalCode::select('id', 'id_m_province', 'regency', 'district', 'village', 'postal_code')
                    ->where([
                        ['id', '!=', 0],
                        ['status', 1],
                    ])
                    ->with(['province.country']);

                    //dd($request);

        if ( $request->province != -1 ) {
            $models->where('id_m_province', $request->province);
        }

        if ($request->has('search')) {
            $keyword = $request->input('search')['value'];
            if (!empty($keyword)) {
                $models->where([
                    [ 'regency', 'like', '%' . $keyword . '%' ],
                ])->orWhere([
                    [ 'district', 'like', '%' . $keyword . '%' ],
                ])->orWhere([
                    [ 'village', 'like', '%' . $keyword . '%' ],
                ])->orWhere([
                    [ 'postal_code', 'like', '%' . $keyword . '%' ],
                ])->orWhereHas('province', function($q) use ($keyword) {
                    $q->where('name', 'like', '%' . $keyword . '%');
                });
            }
        }
        
        $total_record = $models->count();

        $offset = 0;
        $perPage = 50;
        if ($request->has("start")) {
            $start      = $request->input("start");
            $perPage    = $request->input("length");
            $offset     = (($start / $perPage)) * $perPage;

            if ($offset < 0) {
                $offset = 0;
            }
        } 
        $models->skip($offset)->take($perPage);

        $data = [
            'recordsTotal'      => $total_record,
            'recordsFiltered'   => $total_record,
            'per_page'          => $request->has('length') ? $request->input('length') : 50,
            'current_page'      => $request->has('start') ? $request->input('start') : 1,
            'data'              => $models->get()
        ];


        return response()->json($data);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @param  int  $id_m_province
     * @return \Illuminate\Http\Response
     */
    public function get_data_for_select(Request $request)
    {
        if($request->ajax()){
            $models =   PostalCode::select('id', 'id_m_province', 'regency', 'district', 'village', 'postal_code')   
                        ->where('status', 1)
                        ->where(function($q) use ($request) {
                            $q->where('postal_code', 'like', '%' . $request->search . '%')
                            ->orWhere('village', 'like', '%' . $request->search . '%')
                            ->orWhere('district', 'like', '%' . $request->search . '%')
                            ->orWhere('regency', 'like', '%' . $request->search . '%')
                            ->orWhereHas('province', function($q) use ($request) {
                                $q->where('name', 'like', '%' . $request->search . '%');
                            });
                        })
                        ->with('province')
                        ->get();
            
            $result = array();
            $key = 0;
            foreach ($models as $model) {
                $result[$key]['id']     = $model->id;
                $result[$key]['text']   = $model->province->name . ' - ' . $model->regency . ' - ' .$model->district . ' - ' . $model->village . ' - ' . $model->postal_code;
                $key++;
            }

            return response()->json($result);
        }        
    }

}

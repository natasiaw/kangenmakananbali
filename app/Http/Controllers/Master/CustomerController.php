<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Master\Customer;
use App\Models\Master\PostalCode;
use App\Models\Order\CustomerPoint;


class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'master.customer.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer       = new Customer();
        $postalCode     = array();

        return view( 
            'master.customer.form', 
            [
                'model'         => $customer, 
                'postalCode'    => $postalCode,
            ] 
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'full_name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Customer::create(
            [
                'name'              => $request->full_name, 
                'address'           => $request->address, 
                'id_m_postal_code'  => $request->postal_code, 
                'phone'             => $request->phone, 
                'note'              => $request->note, 
                'status'            => (isset( $request->status ) ? 1 : 0)
            ]
        );
        
        return redirect( '/customer' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {

        return view( 
            'master.customer.form', 
            [
                'model'         => $customer, 
            ] 
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $model = Customer::find($customer->id);  

        if ($model) {
                
            $model->name                = $request->full_name;
            $model->address             = $request->address; 
            $model->id_m_postal_code    = $request->postal_code; 
            $model->phone               = $request->phone; 
            $model->note                = $request->note; 
            $model->status              = (isset( $request->status ) ? 1 : 0);

            $model->save();     

        }

        return redirect( route('customer.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data()
    {
        $models =   Customer::where([
                        ['status', 1],
                    ])
                    ->with([
                        'postal_code.province',
                        'point'
                        ])
                    
                    ->get();

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }
}

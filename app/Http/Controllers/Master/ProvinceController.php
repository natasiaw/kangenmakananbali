<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\SupportFacades\Auth;
use Carbon\Carbon;

use App\Models\Master\Province;
use App\Models\Master\Country;

class ProvinceController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'master.province.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $province = new Province();
        $country = Country::select('id', 'name', 'note')->where('status', 1)->get();
        
        return view( 
            'master.province.form', 
            [
                'province' => $province, 
                'country'   => $country
            ] 
        );        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Province::updateOrCreate(
            ['name' => $request->name],
            [
                'id_m_country' => $request->country, 
                'note' => $request->note, 
                'status' => (isset( $request->status ) ? 1 : 0)
            ]
        );

        $this->new_data = $this->serialize_data($model);
        $this->write_log( __CLASS__, $model->id, $request->fullUrl(), __FUNCTION__, $request->ip(), "", $this->new_data );
        
        return redirect( '/province' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function show(Province $province)
    {
        $country = Country::select('id', 'name', 'note')->where('status', 1)->get();
        return view( 
            'master.province.form', 
            [
                'province' => $province, 
                'country'   => $country
            ] 
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function edit(Province $province)
    {
        $country = Country::select('id', 'name', 'note')->where('status', 1)->get();
        return view( 
            'master.province.form', 
            [
                'province' => $province, 
                'country'   => $country
            ] 
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Province $province)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Province::find($province->id);  

        if ($model) {
            $this->old_data = $this->serialize_data($model);
                
            $model->name        = $request->name;
            $model->note        = $request->note;  
            $model->status      = 1; 

            $model->save();     

            $this->new_data = $this->serialize_data($model);
            $this->write_log( __CLASS__, $model->id, $request->fullUrl(), __FUNCTION__, $request->ip(), $this->old_data, $this->new_data );
        }
        
        return redirect( route('province.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Province $province)
    {
        
        $model = Province::find($province->id);

        if ($model) {
            $this->old_data = $this->serialize_data($model);
                
            $model->status      = 0;
            $model->deleted_at  = Carbon::now();
            $model->deleted_by  = auth()->user()->username;

            $model->save();  

            $this->write_log( __CLASS__, $model->id, $request->fullUrl(), __FUNCTION__, $request->ip(), $this->old_data, "" );
        }       

        return response()->json(true);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data()
    {
        $models =   Province::select('id', 'name', 'id_m_country', 'note')
                    ->where([
                        ['id', '!=', 0],
                        ['status', 1],
                    ])
                    ->with(['country'])
                    ->get();

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }
    
    /**
     * Generate json listing of the resource for datatable.
     *
     * @param  int  $id_m_country
     * @return \Illuminate\Http\Response
     */
    public function get_data_by_country(Request $request)
    {
        if($request->ajax()){
            $models = Province::select('id', 'name')->where(['status' => 1, 'id_m_country' => $request->parent])->get();
            
            return view( 
                'template.select', 
                [
                    'models' => $models, 
                ] 
            );
        }        
    }
}

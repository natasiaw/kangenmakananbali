<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Restaurant;
use Illuminate\Http\Request;

class RestaurantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'master.restaurant.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restaurant     = new Restaurant();
        $postalCode     = array();

        return view( 
            'master.restaurant.form', 
            [
                'model'         => $restaurant, 
                'postalCode'    => $postalCode,
            ] 
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Restaurant::create(
            [
                'name'              => $request->name, 
                'address'           => $request->address, 
                'id_m_postal_code'  => $request->postal_code, 
                'phone'             => $request->phone, 
                'note'              => $request->note, 
                'status'            => (isset( $request->status ) ? 1 : 0)
            ]
        );
        
        return redirect( '/restaurant' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function show(Restaurant $restaurant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function edit(Restaurant $restaurant)
    {

        $postalCode     = array();

        return view( 
            'master.restaurant.form', 
            [
                'model'         => $restaurant, 
                'postalCode'    => $postalCode,
            ] 
        );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Restaurant $restaurant)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Restaurant::find($restaurant->id);  

        if ($model) {
                
            $model->name                = $request->name;
            $model->address             = $request->address; 
            $model->id_m_postal_code    = $request->postal_code; 
            $model->phone               = $request->phone; 
            $model->note                = $request->note; 
            $model->status              = 1;

            $model->save();     

        }
        
        return redirect( route('restaurant.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restaurant $restaurant)
    {
        //
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data()
    {
        $models =   Restaurant::where([
                        ['status', 1],
                    ])
                    ->with([
                        'postal_code',
                        ])
                    ->get();

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }
}

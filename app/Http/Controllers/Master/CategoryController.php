<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Category;
use App\Models\Master\CategoryType;
use Carbon\Carbon;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'master.category.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category       = new Category();
        $categoryType   = CategoryType::select('id', 'name')->where('status', 1)->get();

        return view( 
            'master.category.form', 
            [
                'category'         => $category, 
                'categoryType'    => $categoryType,
            ] 
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Category::create(
            [
                'id_m_category_type'    => $request->categoryType,
                'name'                  => $request->name,
                'note'                  => $request->note, 
                'status'                => (isset( $request->status ) ? 1 : 0)
            ]
        );
        
        return redirect( '/category' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view( 'master.category.form', compact('category') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = Category::find($category->id);  

        if ($model) {
            $this->old_data = $this->serialize_data($model);

            $model->name                = $request->name;
            $model->id_m_category_type  = $request->categoryType;
            $model->note                = $request->note; 
            $model->status              = 1;

            $model->save();     

        }
        
        return redirect( route('category.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Category $category)
    {
        $model = Category::find($category->id);

        if ($model) {
            $this->old_data = $this->serialize_data($model);
                
            $model->status      = 0;
            $model->deleted_at  = Carbon::now();
            $model->deleted_by  = auth()->user()->username;  

            $model->save();  

        }       

        return response()->json(true);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data()
    {
        $models =   Category::select('id', 'id_m_category_type', 'name', 'note')
                    ->where([
                        ['status', 1],
                    ])
                    ->with('category_type')
                    ->get();

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }
}

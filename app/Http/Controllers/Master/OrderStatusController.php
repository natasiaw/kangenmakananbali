<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\OrderStatus;
use Illuminate\Http\Request;

class OrderStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( 'master.order_status.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $orderStatus = new OrderStatus();
        return view( 'master.order_status.form', compact('orderStatus') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = OrderStatus::create(
            [
                'name' => $request->name,
                'note' => $request->note, 
                'status' => (isset( $request->status ) ? 1 : 0)
            ]
        );
        
        return redirect( '/orderStatus' );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function show(OrderStatus $orderStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderStatus $orderStatus)
    {
        return view( 'master.order_status.form', compact('orderStatus') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Master\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderStatus $orderStatus)
    {
        request()->validate([
            'name' => ['required', 'min:3', 'max:100']
        ]);

        $model = OrderStatus::find($courier->id);  

        if ($model) {
            $this->old_data = $this->serialize_data($model);
                
            $model->name        = $request->name;
            $model->note        = $request->note; 
            $model->status      = 1;

            $model->save();     

        }
        
        return redirect( route('orderStatus.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, OrderStatus $orderStatus)
    {
        $model = OrderStatus::find($orderStatus->id);

        if ($model) {
            $this->old_data = $this->serialize_data($model);
                
            $model->status      = 0;
            $model->deleted_at  = Carbon::now();
            $model->deleted_by  = auth()->user()->username;  

            $model->save();  

        }       

        return response()->json(true);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data()
    {
        $models =   OrderStatus::select('id', 'name', 'note')
                    ->where([
                        ['id', '!=', 0],
                        ['status', 1],
                    ])
                    ->get();

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }
}

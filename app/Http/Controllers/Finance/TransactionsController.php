<?php

namespace App\Http\Controllers\Finance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Finance\Transactions;
use App\Models\Master\Category;

use Carbon\Carbon;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fromDate   = Transactions::select('date')
                    ->where([
                      ['status', 1]
                    ])
                    ->min('date');

        return view( 'finance.transactions.index', compact('fromDate') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $transactions       = new Transactions();
        $category           = Category::select('id', 'name')->where('status', 1)->get();

        return view( 
            'finance.transactions.form', 
            [
                'category'      => $category, 
                'transactions'  => $transactions,
            ] 
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        for ( $i = 0; $i < count( $request->name ); $i++ )
        {           
            if ( empty( $request->name[$i] )) {
                continue;
            }

            $model = Transactions::create([
                'id_m_category' => $request->category[$i],
                'date'          => Carbon::createFromFormat( config('global.datepicker_date_format'), $request->effective_date[$i] )->timestamp,
                'name'          => $request->name[$i],
                'amount'        => $request->amount[$i],
            ]);
    
        }
        
        return redirect( route( 'transactions.index' ) );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Finance\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function show(Transactions $transactions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Finance\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function edit(Transactions $transaction)
    {
        $category           = Category::select('id', 'name')->where('status', 1)->get();

        return view( 
            'finance.transactions.edit', 
            [
                'model'         => $transaction, 
                'category'      => $category,
            ] 
        );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Finance\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transactions $transaction)
    {
        $model = Transactions::find($transaction->id);  

        if ($model) {
                
            $model->date        = Carbon::createFromFormat( config('global.datepicker_date_format'),$request->transaction_date)->timestamp;
            $model->name        = $request->name;
            $model->amount      = $request->amount;
            $model->note        = $request->note;

            $model->save();     

        }
        
        return redirect( route('transactions.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Finance\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transactions $transaction)
    {
        $model = Transactions::find($transaction->id);

        if ($model) {
                
            $model->status      = 0;
            $model->deleted_at  = Carbon::now();
            $model->deleted_by  = auth()->user()->username;  

            $model->save();  

        }       

        return response()->json(true);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data(Request $request)
    {
      
        $from_date = Carbon::createFromFormat( config('global.datepicker_date_format'), $request->from_date );
        $to_date = Carbon::createFromFormat( config('global.datepicker_date_format'), $request->to_date );

        $models =   Transactions::where([
                        ['status', 1],
                        ['date', '>=', $from_date->setTime(0,0,0)->timestamp],
                        ['date', '<=', $to_date->setTime(23,59,59)->timestamp],
                    ])
                    ->with('category.category_type')

                    ->get();

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }
}

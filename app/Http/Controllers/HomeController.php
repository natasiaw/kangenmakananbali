<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Order\CustomerOrder;

use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$user = Auth::user()->load('jobTitle');
        
        return view('home');
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_orders_classification(Request $request)
    {
        
        $models =  DB::select( 
            DB::raw("
                SELECT SUM(od.quantity) AS total, m.name AS menu, r.name AS restaurant
                FROM order_detail od
                LEFT JOIN customer_order co ON od.id_customer_order = co.id
                LEFT JOIN m_menu m ON od.m_menu_id = m.id
                LEFT JOIN m_restaurant r ON m.m_restaurant_id = r.id
                WHERE co.status = 1 AND co.m_order_status_id = 1 OR co.m_order_status_id = 10
                GROUP BY m.name, r.name
                ORDER BY r.name 
            ") 
        );

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_orders_date(Request $request)
    {
        $models =   CustomerOrder::where([
            ['status', 1],
            ['actual_shipping_date', '>=', Carbon::now()->setTime(0,0,0)->timestamp]
        ])->orderBy('actual_shipping_date', 'asc')
        ->first();

        return response()->json($models);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_total_price(Request $request)
    {
        $models =   CustomerOrder::where([
            ['status', 1],
            ['m_order_status_id', 1],
            ['actual_shipping_date', '>=', Carbon::now()->setTime(0,0,0)->timestamp]
        ])
        ->orWhere([
            ['m_order_status_id', 10]
        ])
        ->with([
            'order_detail'
        ])
        ->get();

        return response()->json($models);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_orders_summary(Request $request)
    {
      
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_data_complete_not_paid(Request $request)
    {
        $models =   CustomerOrder::where([
                        ['status', 1],
                        ['m_order_status_id', 9]
                    ])
                    ->with([
                        'customer.postal_code.province',
                        'order_status',
                        'order_detail.menu.restaurant'
                        ]);

        $total_record = $models->count();

        $offset = 0;
        $perPage = 50;
        if ($request->has("start")) {
            $start      = $request->input("start");
            $perPage    = $request->input("length");
            $offset     = (($start / $perPage)) * $perPage;

            if ($offset < 0) {
                $offset = 0;
            }
        } 
        $models->skip($offset)->take($perPage);

        $data = [
            'recordsTotal'      => $total_record,
            'recordsFiltered'   => $total_record,
            'per_page'          => $request->has('length') ? $request->input('length') : 50,
            'current_page'      => $request->has('start') ? $request->input('start') : 1,
            'data'              => $models->get()
        ];


        return response()->json($data);
    }
   
}

<?php

namespace App\Http\Controllers\Sales;

use App\Http\Controllers\Controller;
use App\Models\Sales\Report;
use Illuminate\Http\Request;
use App\Models\Finance\Transactions;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fromDate   = Transactions::select('date')
                    ->where([
                      ['status', 1]
                    ])
                    ->min('date');

        return view( 'finance.report.index', compact('fromDate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sales\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sales\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sales\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sales\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        //
    }


    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_profit_loss(Request $request)
    {
        $models =  DB::select( 
            DB::raw("
                SELECT ct.name AS TYPE, c.name AS Category, MONTH(FROM_UNIXTIME(t.DATE)) AS MONTH, SUM(t.amount) 
                FROM transactions t
                LEFT JOIN m_category c ON t.`id_m_category` = c.id
                LEFT JOIN m_category_type ct ON c.`id_m_category_type` = ct.id
                WHERE t.status = 1
                GROUP BY c.name, MONTH(FROM_UNIXTIME(t.DATE)), ct.name
                ORDER BY MONTH
            ") 
        );

        $data = [
            'data' => $models
        ];

        return response()->json($data);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_total_customer(Request $request)
    {
        $from_date = Carbon::createFromFormat( config('global.datepicker_date_format'), $request->from_date)->format('Y-m-d');
        $to_date   = Carbon::createFromFormat( config('global.datepicker_date_format'), $request->to_date)->addDay()->format('Y-m-d');


        $models =  DB::select( 
            DB::raw("
            SELECT COUNT(*) as total FROM m_customer
            WHERE STATUS = 1
            AND created_at >= '$from_date'
            AND created_at <= '$to_date'
            ") 
        );

        return response()->json($models);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_report(Request $request)
    {
        $from_date = Carbon::createFromFormat( config('global.datepicker_date_format'), $request->from_date )->setTime(0,0,0)->timestamp;
        $to_date = Carbon::createFromFormat( config('global.datepicker_date_format'), $request->to_date )->setTime(23,59,59)->timestamp;

        $models =  DB::select( 
            DB::raw("
                SELECT 'total_expenses', sum(amount) total
                from transactions t
                left join m_category c on t.id_m_category = c.id
                left join m_category_type ct on c.id_m_category_type = ct.id
                where date >= '$from_date'
                and DATE <= '$to_date'
                and ct.id = 4
                UNION
                SELECT 'gross_sales', SUM(amount) total
                FROM transactions t
                LEFT JOIN m_category c ON t.id_m_category = c.id
                WHERE DATE >= '$from_date'
                AND DATE <= '$to_date'
                AND c.id = 8
                union
                SELECT 'total_cost_of_sales', SUM(amount) total
                FROM transactions t
                LEFT JOIN m_category c ON t.id_m_category = c.id
                LEFT JOIN m_category_type ct ON c.id_m_category_type = ct.id
                WHERE DATE >= '$from_date'
                AND DATE <= '$to_date'
                AND ct.id = 5
                UNION
                SELECT 'ads', SUM(amount) total
                FROM transactions t
                LEFT JOIN m_category c ON t.id_m_category = c.id
                WHERE DATE >= '$from_date'
                AND DATE <= '$to_date'
                AND c.id = 11
                UNION
                SELECT 'postage_sales', SUM(amount) total
                FROM transactions t
                LEFT JOIN m_category c ON t.id_m_category = c.id
                WHERE DATE >= '$from_date'
                AND DATE <= '$to_date'
                AND c.id = 9
            ") 
        );

        return response()->json($models);
    }

    public function get_profit_per_month() {   

        $models = DB::select("SELECT x.month AS name, (x.gross - x.cost_of_sales - x.expenses) AS y
        FROM
        (
        SELECT a.month,
        COALESCE(a.expenses, 0) AS `expenses`, 
        COALESCE(a.gross, 0) AS `gross`, 
        COALESCE(a.cost_of_sales, 0) AS `cost_of_sales`,
        COALESCE(a.ads, 0) AS `ads`,
        COALESCE(a.postage_sales, 0) AS `postage_sales`
        FROM (
            SELECT SUM(CASE WHEN ct.id = 4 THEN t.amount END) AS expenses, 
            SUM(CASE WHEN c.id = 8 THEN t.amount END) AS gross, 
            SUM(CASE WHEN ct.id = 5 THEN t.amount END) AS cost_of_sales, 
            SUM(CASE WHEN c.id = 11 THEN t.amount END) AS ads, 
            SUM(CASE WHEN c.id = 9 THEN t.amount END) AS postage_sales, 
            MONTHNAME(FROM_UNIXTIME(DATE)) AS MONTH
            FROM transactions t
            LEFT JOIN m_category c ON t.id_m_category = c.id
            LEFT JOIN m_category_type ct ON c.id_m_category_type = ct.id
            GROUP BY MONTHNAME(FROM_UNIXTIME(DATE))
            ORDER BY MONTH(FROM_UNIXTIME(DATE)) asc
        ) a) X");

        return response()->json($models);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_customer_per_month()
    {
        $models =  DB::select( 
            DB::raw("
                SELECT COUNT(*) AS y, MONTHNAME(created_at) AS name
                FROM m_customer
                WHERE STATUS = 1
                GROUP BY MONTHNAME(created_at)
                ORDER BY MONTH(created_at)
            ") 
        );

        return response()->json($models);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_expenses_per_month()
    {
        $models =  DB::select( 
            DB::raw("
                SELECT SUM(CASE WHEN ct.id = 4 THEN t.amount END) AS y,
                MONTHNAME(FROM_UNIXTIME(DATE)) AS name
                FROM transactions t
                LEFT JOIN m_category c ON t.id_m_category = c.id
                LEFT JOIN m_category_type ct ON c.id_m_category_type = ct.id
                GROUP BY MONTHNAME(FROM_UNIXTIME(DATE))
                ORDER BY MONTH(FROM_UNIXTIME(DATE)) ASC
            ") 
        );

        return response()->json($models);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_ads_per_month()
    {
        $models =  DB::select( 
            DB::raw("
                SELECT SUM(t.amount) AS y,
                MONTHNAME(FROM_UNIXTIME(DATE)) AS name
                FROM transactions t
                RIGHT JOIN m_category c ON t.id_m_category = c.id
                WHERE c.id = 11
                GROUP BY MONTHNAME(FROM_UNIXTIME(DATE))
                ORDER BY MONTH(FROM_UNIXTIME(DATE)) ASC
            ") 
        );

        return response()->json($models);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_gross_per_month()
    {
        $models =  DB::select( 
            DB::raw("
                SELECT SUM(t.amount) AS y,
                MONTHNAME(FROM_UNIXTIME(DATE)) AS name
                FROM transactions t
                RIGHT JOIN m_category c ON t.id_m_category = c.id
                WHERE c.id = 8
                GROUP BY MONTHNAME(FROM_UNIXTIME(DATE))
                ORDER BY MONTH(FROM_UNIXTIME(DATE)) ASC
            ") 
        );

        return response()->json($models);
    }

    /**
     * Generate json listing of the resource for datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function get_net_per_month()
    {
        $models =  DB::select( 
            DB::raw("
                SELECT x.month AS name, (x.gross - x.cost_of_sales) AS y
                FROM
                (
                SELECT a.month,
                COALESCE(a.expenses, 0) AS `expenses`, 
                COALESCE(a.gross, 0) AS `gross`, 
                COALESCE(a.cost_of_sales, 0) AS `cost_of_sales`,
                COALESCE(a.ads, 0) AS `ads`,
                COALESCE(a.postage_sales, 0) AS `postage_sales`
                FROM (
                    SELECT SUM(CASE WHEN ct.id = 4 THEN t.amount END) AS expenses, 
                    SUM(CASE WHEN c.id = 8 THEN t.amount END) AS gross, 
                    SUM(CASE WHEN ct.id = 5 THEN t.amount END) AS cost_of_sales, 
                    SUM(CASE WHEN c.id = 11 THEN t.amount END) AS ads, 
                    SUM(CASE WHEN c.id = 9 THEN t.amount END) AS postage_sales, 
                    MONTHNAME(FROM_UNIXTIME(DATE)) AS MONTH
                    FROM transactions t
                    LEFT JOIN m_category c ON t.id_m_category = c.id
                    LEFT JOIN m_category_type ct ON c.id_m_category_type = ct.id
                    GROUP BY MONTHNAME(FROM_UNIXTIME(DATE))
                    ORDER BY MONTH(FROM_UNIXTIME(DATE)) ASC
                ) a) X
            ") 
        );

        return response()->json($models);
    }
}

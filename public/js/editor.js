(function($) {
  'use strict';
  /*Quill editor*/
  if ($("#quill-note").length) {
    var quill = new Quill('#quill-note', {
      modules: {
        toolbar: [
          [{
            header: [1, 2, false]
          }],
          ['bold', 'italic', 'underline', 'strike'],
          ['image']
        ]
      },
      placeholder: 'Description',
      theme: 'snow' // or 'bubble'
    });
  }

  if ($("#approval-note").length) {
    var quill = new Quill('#approval-note', {
      modules: {
        toolbar: [
          [{
            header: [1, 2, false]
          }],
          ['bold', 'italic', 'underline', 'strike'],
          ['image']
        ]
      },
      placeholder: 'Description',
      theme: 'snow' // or 'bubble'
    });
  }

})(jQuery);
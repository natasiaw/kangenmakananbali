jQuery(document).ready(function(){

  jQuery('select.select2e').each(function(){
    var input = jQuery(this);      
    var route = input.attr('select2_route');
    var token = jQuery("input[name='_token']").val();

    input.select2({
      minimumInputLength: 3,
      allowClear: true,
      placeholder: 'search...',
      ajax: {
        dataType: 'json',
        type: "POST",
        url: route,
        data: function(params) {
          return {
            search: params.term,
            _token: token,
          }
        },
        processResults: function (data, page) {
          return {
            results: data
          };
        },
        error: function (data) {
          console.log(data.responseText);
        },
      }
    });

  });

});
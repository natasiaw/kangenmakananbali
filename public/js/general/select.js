jQuery(document).ready(function(){
  




  jQuery('select.select2').each(function(){
    jQuery(this).select2();
    
  });

  jQuery('select.mpselect').each(function(){
    var input = jQuery(this);      

    input.change(function(){
      
      var token       = jQuery("input[name='_token']").val();
      var parent_id   = input.val();
      var child_route = input.attr('child_route');
      var child_name  = input.attr('child_name');
      var child_init  = input.attr('child_init');

      if (child_route) {
        jQuery.ajax({
          url: child_route,
          method: 'POST',
          data: { parent:parent_id, _token:token },
          success: function(data) {
            jQuery("select[name='" + child_name + "'").html('');
            jQuery("select[name='" + child_name + "'").html(data);

            if (child_init) {
              input.removeAttr('child_init');
              jQuery("select[name='" + child_name + "']").val(child_init).change();
            } else {           
              jQuery("select[name='" + child_name + "']").change();
            }

          }
        });
      }      
    });
  });

  jQuery('select.mpselect').each(function(){
    jQuery(this).select2({closeOnSelect: false});
    var input   = jQuery(this);      
    var init    = input.attr('init');

    if (init) {
      input.removeAttr('init');
      input.val(init).change();
    }
  }); 

});
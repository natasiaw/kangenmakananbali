jQuery(document).ready(function(){
  jQuery('.simpleMde').each(function(i){
    var editor = new SimpleMDE({
      element: this,
      hideIcons: ['guide', 'heading', 'side-by-side', 'fullscreen'],
      showIcons: ['strikethrough', 'table'],
      autoRefresh:true,
      spellChecker: false,
    });
    editor.codemirror.options.extraKeys['Tab'] = false;
    editor.codemirror.options.extraKeys['Shift-Tab'] = false;
  });    

  jQuery('.modal.fade').each(function(){
    jQuery(this).on('shown.bs.modal', function (event) {
      jQuery(this).find('textarea').nextAll('.CodeMirror')[0].CodeMirror.refresh();
    })
  });

});
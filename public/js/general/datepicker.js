var DATE_FORMAT_DATEPICKER    = 'dd/mm/yyyy';

jQuery(document).ready(function(){

  jQuery('.datepicker').each(function(){
    jQuery(this).datepicker({
      format: DATE_FORMAT_DATEPICKER,
      todayBtn: true,
      clearBtn: true,
      autoclose: true,
      todayHighlight: true,
      forceParse: false,
    });
  });

  jQuery('.datepickermonth').each(function(){
    jQuery(this).datepicker({
      format: "mm/yyyy",
      viewMode: "months", 
      minViewMode: "months",
      clearBtn: true,
      autoclose: true,
      forceParse: false,
    });
  });

  jQuery('.datepicker-multi').each(function(){
    jQuery(this).datepicker({
      format: DATE_FORMAT_DATEPICKER,
      todayBtn: true,
      clearBtn: true,
      //autoclose: true,
      todayHighlight: true,
      forceParse: false,
      multidate: true,
      multidateSeparator: ', ',
    });
  });

});
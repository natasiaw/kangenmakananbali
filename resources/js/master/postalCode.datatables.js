(function($) {
  (function() {  
    var formData;
    formData = {
      province : jQuery('#province').val(),
    }

    var t = $('#table-listing').DataTable({
      processing: true,
      serverSide: true,
      scrollX: true,
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      dom: 'Bfrtip',
      buttons: [
          'pageLength', 
          {
            extend: 'copy',
            exportOptions: { 
              columns: [ 0, 1, 2 ], 
              rows: ':visible' 
            }
          },
          {
            extend: 'csv',
            exportOptions: { 
              columns: [ 0, 1, 2 ], 
              rows: ':visible' 
            }
          },
          {
            extend: 'excel',
            exportOptions: { 
              columns: [ 0, 1, 2 ], 
              rows: ':visible' 
            }
          },
          {
            extend: 'pdf',
            exportOptions: { 
              columns: [ 0, 1, 2 ], 
              rows: ':visible' 
            },
            customize: function (doc) {
              doc.content[1].table.widths = 
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
          },
          {
            extend: 'print',
            exportOptions: { 
              columns: [ 0, 1, 2 ], 
              rows: ':visible' 
            }
          }
      ],
      ordering: true,
      order: [],
      pageLength: 50,
      ajax: {
          type: "GET",
          url: data_route,
          data: function ( d ) {
            d.province = jQuery('#province').val();
          },
          dataType: "json",
          'error': function (xhr, textStatus, ThrownException) {
            console.log(xhr.responseText);
            // alert('Error loading data. Exception: ' + ThrownException + "\n" + textStatus);
          }
      },
      columns: [
        {
          data: "id", 
          defaultContent: '',
          searchable: false,
          orderable: false,
        },
        { data: "province.name" },
        { data: "regency" },
        { data: "district" },
        { data: "village" },
        { data: "postal_code" },
        {
          data: 'id',
          render: function(data, type, row) {
            var string_button = "";
           
            //string_button += '<a href="' + url_base + "/" + data + '/edit" class="text-primary mr-1 p-2"><i class="mdi mdi-lead-pencil"></i></a>';
            string_button += '<a href="" onclick="showSwal(' + data + ')" class="text-danger mr-1 p-2"><i class="mdi mdi-delete"></i></a>';
            return string_button;
          },
          orderable: false
        }
      ],
      fixedColumns: true
    });

    /*t.on('draw.dt', function () {
      var info = t.page.info();
      t.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1 + info.start;
      });
    });*/

    jQuery('#province').change(function(){
      t.ajax.reload();
    });   

    $.ajaxSetup({
      headers: {
        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
      }
    });
    
    showSwal = function(item_id) {
      event.preventDefault();

      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        buttons: {
          cancel: {
            text: "Cancel",
            value: null,
            visible: true,
            closeModal: true,
          },
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            closeModal: true,
          }
        }
      }).then((result) => {
        if (result) {
          var token = $('[name="_token"]').val();
          $.post(url_base + '/' + item_id, {"_method" : "DELETE", "_token" : token}, function(response) {
            if (response == true) {
              swal({
                title: 'Deleted!',
                text: 'Item has been deleted.',
                icon: 'success'
              }).then((result) => {
                window.location.reload();
              });    
            } else {
              swal({
                title: 'Delete failed!',
                text: 'Item is not deleted.',
                icon: 'error'
              })
            }
            
          });
        } 
      })
    };

  }());
})(jQuery);
@extends('template.main')
@section('main-content')
<meta name="_token" content="{{ csrf_token() }}">
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Sales Report</h4>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-md-12">
		@include('template.error')

		<div class="row flex-grow">
			<div class="col-12 col-md-12 col-lg-6 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="form-group row ml-0 mb-0">
							<label for="report_date" class="col-sm-2 col-form-label">From</label>
							<div class="col-sm-8">
								<div id="from_date-popup" class="input-group date datepicker">
								<input type="text" class="form-control" id="from_date" name="from_date" placeholder="dd/mm/yyyy" value="{{ Carbon\Carbon::createFromTimestamp( $fromDate )->format('d/m/Y') }}">
								<span class="mp-date-format">dd/mm/yyyy</span>
								<span class="input-group-addon input-group-append border-left">
									<span class="mdi mdi-calendar input-group-text"></span>
								</span>                   
								</div>
							</div>     
						</div>	
					</div>
				</div>
			</div>
			<div class="col-12 col-md-12 col-lg-6 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="form-group row ml-0 mb-0">
							<label for="report_date" class="col-sm-2 col-form-label">To</label>
							<div class="col-sm-8">
								<div id="to_date-popup" class="input-group date datepicker">
									<input type="text" class="form-control" id="to_date" name="to_date" placeholder="dd/mm/yyyy" value="{{ Carbon\Carbon::now()->format('d/m/Y') }}">
									<span class="mp-date-format">dd/mm/yyyy</span>
									<span class="input-group-addon input-group-append border-left">
										<span class="mdi mdi-calendar input-group-text"></span>
									</span>                   
								</div>
							</div>    
						</div>
					</div>
				</div>
			</div>
			
		</div>

		<div class="row flex-grow">
			<div class="col-12 col-md-12 col-lg-4 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Customer</h4>
							<h2 id="total_customer"></h2>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-12 col-lg-4 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Net Sales</h4>
							<h2 id="net_sales"></h2>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col-12 col-md-12 col-lg-4 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Profit</h4>
							<h2 id="total_profit"></h2>
						</div>
						
					</div>
				</div>
			</div>
			
		</div>

		

		<div class="row flex-grow">
			<div class="col-12 col-md-12 col-lg-4 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Gross Sales</h4>
							<h2 id="gross_sales"></h2>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col-12 col-md-12 col-lg-4 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Postage Sales</h4>
							<h2 id="postage_sales"></h2>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col-12 col-md-12 col-lg-4 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Gross Margin</h4>
							<h2 id="total_cost_of_sales"></h2>
						</div>
					</div>
				</div>
			</div>
			
			
		</div>

		<div class="row flex-grow">
			<div class="col-12 col-md-12 col-lg-4 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Total Expenses</h4>
							<h2 id="total_expenses"></h2>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-12 col-lg-4 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Ads</h4>
							<h2 id="total_ads"></h2>
						</div>
						
					</div>
				</div>
			</div>
			
			
		</div>
		<div class="row flex-grow">
			<div class="col-12 col-md-12 col-lg-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Profit per month</h4>
							<div class="dropdown dropleft card-menu-dropdown">
								<button class="btn p-0" type="button" id="profit_per_month" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="mdi mdi-dots-vertical card-menu-btn"></i>
								</button>
							</div>
						</div>
						<div class="mt-4 mb-4 d-sm-flex" id="chart_profit_per_month"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row flex-grow">
			<div class="col-12 col-md-12 col-lg-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Customer per month</h4>
							<div class="dropdown dropleft card-menu-dropdown">
								<button class="btn p-0" type="button" id="customer_per_month" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="mdi mdi-dots-vertical card-menu-btn"></i>
								</button>
							</div>
						</div>
						<div class="mt-4 mb-4 d-sm-flex" id="chart_customer_per_month"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row flex-grow">
			<div class="col-12 col-md-12 col-lg-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Expenses per month</h4>
							<div class="dropdown dropleft card-menu-dropdown">
								<button class="btn p-0" type="button" id="customer_per_month" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="mdi mdi-dots-vertical card-menu-btn"></i>
								</button>
							</div>
						</div>
						<div class="mt-4 mb-4 d-sm-flex" id="chart_expenses_per_month"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row flex-grow">
			<div class="col-12 col-md-12 col-lg-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Ads per month</h4>
							<div class="dropdown dropleft card-menu-dropdown">
								<button class="btn p-0" type="button" id="ads_per_month" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="mdi mdi-dots-vertical card-menu-btn"></i>
								</button>
							</div>
						</div>
						<div class="mt-4 mb-4 d-sm-flex" id="chart_ads_per_month"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row flex-grow">
			<div class="col-12 col-md-12 col-lg-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Gross sales per month</h4>
							<div class="dropdown dropleft card-menu-dropdown">
								<button class="btn p-0" type="button" id="gross_per_month" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="mdi mdi-dots-vertical card-menu-btn"></i>
								</button>
							</div>
						</div>
						<div class="mt-4 mb-4 d-sm-flex" id="chart_gross_per_month"></div>
					</div>
				</div>
			</div>
		</div>

		<div class="row flex-grow">
			<div class="col-12 col-md-12 col-lg-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Net sales per month</h4>
							<div class="dropdown dropleft card-menu-dropdown">
								<button class="btn p-0" type="button" id="net_per_month" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="mdi mdi-dots-vertical card-menu-btn"></i>
								</button>
							</div>
						</div>
						<div class="mt-4 mb-4 d-sm-flex" id="chart_net_per_month"></div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>			
@endsection
@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endsection
@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/highcharts.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/general/exports-highchart.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/general/exportdata-highchart.js') }}"></script>
<script type="text/javascript">

$(document).ready(function() {
	get_customer();
	get_report();
	drawChart_profit_per_month();
	drawChart_customer_per_month();
	drawChart_expenses_per_month();
	drawChart_ads_per_month();
	drawChart_gross_per_month();
	drawChart_net_per_month();

	jQuery('#from_date').change(function(){
		get_customer();
		get_report();
    });  

    jQuery('#to_date').change(function(){
		get_customer();
		get_report();
    });  

	function formatNumber(num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
	}

	function get_customer() {
		var date_route    = "{{ route('reportSales.getcustomer') }}";
		var token       = jQuery("input[name='_token']").val();
		var from 		= jQuery('#from_date').val();
		var to 			= jQuery('#to_date').val();
		jQuery.ajax({
			method: "POST",
			url: date_route,
			data: { from_date:from, to_date:to,  _token:token },
			success: function(datas) {
				jQuery("#total_customer").html(datas[0].total);
			}
		});	
	}

	function get_report() {
		var date_route	= "{{ route('reportSales.getreport') }}";
		var token       = jQuery("input[name='_token']").val();
		var from 		= jQuery('#from_date').val();
		var to 			= jQuery('#to_date').val();
		jQuery.ajax({
			method: "POST",
			url: date_route,
			data: { from_date:from, to_date:to,  _token:token },
			dataType: "json",
			success: function(datas) {
				var gross_sales = 0;
				if(datas[1].total != null){
					gross_sales = datas[1].total;
				}
				jQuery("#gross_sales").html("Rp " + formatNumber(gross_sales));

				var total_cost_of_sales = 0;
				if(datas[2].total != null){
					total_cost_of_sales = datas[2].total;
				}
				jQuery("#total_cost_of_sales").html("Rp " + formatNumber(datas[2].total));

				var total_expenses = 0;
				if(datas[0].total != null){
					total_expenses = datas[0].total;
				}
				jQuery("#total_expenses").html("Rp " + formatNumber(total_expenses));

				var ads = 0;
				if(datas[3].total != null){
					ads = datas[3].total;
				}
				jQuery("#total_ads").html("Rp " + formatNumber(ads));

				var postage_sales = 0;
				if(datas[4].total != null){
					postage_sales = datas[4].total;
				}
				jQuery("#postage_sales").html("Rp " + formatNumber(postage_sales));

				var net_sales=0;
				net_sales = gross_sales - total_cost_of_sales;
				jQuery("#net_sales").html("Rp " + formatNumber(net_sales));

				var total_profit=0;
				total_profit = net_sales - total_expenses;
				jQuery("#total_profit").html("Rp " + formatNumber(total_profit));

				
			},
			error: function(xhr, textStatus, ThrownException) {
				console.log(xhr.responseText);
			}
		});	
	}

	function drawChart_profit_per_month() {
		$.ajax({
			url: "{{ route( 'reportSales.getprofitpermonth' ) }}",
            dataType: "json",
            success: function(datas) {
			
				Highcharts.chart('chart_profit_per_month', {
					chart: {type: 'column'},
					title: {
						useHTML: true,
						text: '&nbsp;'
						},
					xAxis: {type: 'category'},
					legend: {enabled: false},
					plotOptions: {
						series: {
							borderWidth: 0,
							dataLabels: {
								enabled: true
							}
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
						pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
					},
					series: [
                        {
							name: "Total",
							colorByPoint: true,
                            data: datas
                        }
                    ]
				});	
            }
		});
	}

	function drawChart_customer_per_month() {
		$.ajax({
			url: "{{ route( 'reportSales.getcustomerpermonth' ) }}",
            dataType: "json",
            success: function(datas) {
			
				Highcharts.chart('chart_customer_per_month', {
					chart: {type: 'column'},
					title: {
						useHTML: true,
						text: '&nbsp;'
						},
					xAxis: {type: 'category'},
					legend: {enabled: false},
					plotOptions: {
						series: {
							borderWidth: 0,
							dataLabels: {
								enabled: true
							}
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
						pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
					},
					series: [
                        {
							name: "Total",
							colorByPoint: true,
                            data: datas
                        }
                    ]
				});	
            }
		});
	}

	function drawChart_expenses_per_month() {
		$.ajax({
			url: "{{ route( 'reportSales.getexpensespermonth' ) }}",
            dataType: "json",
            success: function(datas) {
			
				Highcharts.chart('chart_expenses_per_month', {
					chart: {type: 'column'},
					title: {
						useHTML: true,
						text: '&nbsp;'
						},
					xAxis: {type: 'category'},
					legend: {enabled: false},
					plotOptions: {
						series: {
							borderWidth: 0,
							dataLabels: {
								enabled: true
							}
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
						pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
					},
					series: [
                        {
							name: "Total",
							colorByPoint: true,
                            data: datas
                        }
                    ]
				});	
            }
		});
	}

	function drawChart_ads_per_month() {
		$.ajax({
			url: "{{ route( 'reportSales.getadspermonth' ) }}",
            dataType: "json",
            success: function(datas) {
			
				Highcharts.chart('chart_ads_per_month', {
					chart: {type: 'column'},
					title: {
						useHTML: true,
						text: '&nbsp;'
						},
					xAxis: {type: 'category'},
					legend: {enabled: false},
					plotOptions: {
						series: {
							borderWidth: 0,
							dataLabels: {
								enabled: true
							}
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
						pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
					},
					series: [
                        {
							name: "Total",
							colorByPoint: true,
                            data: datas
                        }
                    ]
				});	
            }
		});
	}

	function drawChart_gross_per_month() {
		$.ajax({
			url: "{{ route( 'reportSales.getgrosspermonth' ) }}",
            dataType: "json",
            success: function(datas) {
			
				Highcharts.chart('chart_gross_per_month', {
					chart: {type: 'column'},
					title: {
						useHTML: true,
						text: '&nbsp;'
						},
					xAxis: {type: 'category'},
					legend: {enabled: false},
					plotOptions: {
						series: {
							borderWidth: 0,
							dataLabels: {
								enabled: true
							}
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
						pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
					},
					series: [
                        {
							name: "Total",
							colorByPoint: true,
                            data: datas
                        }
                    ]
				});	
            }
		});
	}

	function drawChart_net_per_month() {
		$.ajax({
			url: "{{ route( 'reportSales.getnetpermonth' ) }}",
            dataType: "json",
            success: function(datas) {
			
				Highcharts.chart('chart_net_per_month', {
					chart: {type: 'column'},
					title: {
						useHTML: true,
						text: '&nbsp;'
						},
					xAxis: {type: 'category'},
					legend: {enabled: false},
					plotOptions: {
						series: {
							borderWidth: 0,
							dataLabels: {
								enabled: true
							}
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
						pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
					},
					series: [
                        {
							name: "Total",
							colorByPoint: true,
                            data: datas
                        }
                    ]
				});	
            }
		});
	}

});
</script>			
@endsection



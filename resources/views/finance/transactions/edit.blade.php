@extends('template.main')
@section('main-content')
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Transactions</h4>
      </div>
    </div>
  </div>
</div>

<form class="mpform" id="mpform" method="POST" action="{{ route('transactions.update', ['transaction' => $model->id]) }}">
  {{ method_field('PATCH') }}           
  @csrf

  <input type="hidden" id="id" name="id" value="{{ $model->id }}">

  @include('template.error')

<div class="row">
  <div class="col-lg-6 d-flex flex-column">
    <div class="row">

      <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">       
            <div class="form-group row">
              <label for="category" class="col-sm-3 col-form-label">Category</label>
              <div class="col-sm-9">
                <select name="category" class="form-control select2">
                  @foreach($category as $s)
                  <option value="{{ $s->id }}" @if($model->id_m_category == $s->id) selected @endif>{{ $s->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="transaction_date" class="col-sm-3 col-form-label">Date</label>
              <div class="col-sm-9">
                <div id="transaction_date-popup" class="input-group date datepicker">
                  <input type="text" class="form-control" id="transaction_date" name="transaction_date" placeholder="dd/mm/yyyy" value="@if( isset($model->id) ) {{ Carbon\Carbon::createFromTimestamp( $model->date )->format('d/m/Y') }} @else {{ Carbon\Carbon::now() }} @endif">
                  <span class="mp-date-format">dd/mm/yyyy</span>
                  <span class="input-group-addon input-group-append border-left">
                    <span class="mdi mdi-calendar input-group-text"></span>
                  </span>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="name" class="col-sm-3 col-form-label">Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" placeholder="Name" value="{{ $errors->has('name') ? old('name' ) : $model->name }}">
              </div>
            </div>
            <div class="form-group row">
              <label for="amount" class="col-sm-3 col-form-label">Amount</label>
              <div class="col-sm-9">
                <input type="text" class="form-control {{ $errors->has('amount') ? 'is-invalid' : '' }}" id="amount" name="amount" placeholder="Price Original" value="{{ $errors->has('amount') ? old('price_original' ) : $model->amount }}">
              </div>
            </div>
            
            <div class="form-group row">
              <label for="note" class="col-sm-3 col-form-label">Description</label>
              <div class="col-sm-9">
                <textarea name="note" id="note" class="simpleMde">{!! $model->note !!}</textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 d-flex flex-column">

    <div class="card">
      <div class="card-body">          
        <div class="form-group float-right mb-0">        
          <a href="{{ URL::previous() }}"><button type="button" class="btn btn-lg btn-light mr-3" id="cancel" name="cancel">Cancel</button></a>     
          <button type="submit" class="btn btn-lg btn-primary">Save</button>
        </div>
      </div>
    </div>

  </div>
</div> 

</form>
@endsection

@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/simplemde/simplemde.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/simplemde/simplemde.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/select.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/select2e.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/editor.js') }}"></script>
@endsection
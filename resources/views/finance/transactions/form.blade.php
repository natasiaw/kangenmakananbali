@extends('template.main')
@section('main-content')
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Transactions</h4>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 col-md-12 d-flex flex-column">
  
  @include('template.error')
  
    <div class="card">
      <div class="card-body">
        
        <form class="mpform" id="mpform" method="POST" action="@if( isset($transactions->id) ){{ route('transactions.update', ['transactions' => $transactions->id]) }}@else{{ route( 'transactions.store' ) }}@endif">
          @if( isset($transactions->id) ) {{ method_field('PATCH') }} @else {{ method_field('POST') }} @endif          
          @csrf
          <input type="hidden" id="id" name="id" value="{{ $transactions->id }}">
          <div class="row">
            <div class="col-sm-12">
              <table class="table table-hover" id="mp_table">
                <thead>
                  <tr>
                    <th style="width:25%;">Category</th>
                    <th style="width:25%;">Date</th>
                    <th style="width:30%;">Name</th>
                    <th style="width:20%;">Amount</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <select id="category" name="category[]" class="form-control select2">
                        @foreach($category as $s)
                        <option value="{{ $s->id }}">{{ $s->name }}</option>
                        @endforeach
                      </select>
                    </td>
                    <td>
                      <div id="effective_date-popup" class="input-group date datepicker">
                        <input type="text" class="form-control" id="effective_date" name="effective_date[]" placeholder="dd/mm/yyyy" value="{{ Carbon\Carbon::now()->format('d/m/Y') }}">
                        <span class="mp-date-format">dd/mm/yyyy</span>
                        <span class="input-group-addon input-group-append border-left">
                          <span class="mdi mdi-calendar input-group-text"></span>
                        </span>
                      </div>
                    </td>
                    <td>
                      <input type="text" class="form-control" id="name" name="name[]" placeholder="desc" value="">
                    </td>
                    <td>
                      <input type="number" class="form-control" id="amount" name="amount[]" placeholder="amount" value="">
                    </td>
                    <td>
                      <a href="#" class="text-primary mr-1 p-2" id="add_row"><i class="mdi mdi-plus"></i></a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>          
          
          <div class="form-group mt-5 float-right">             
            <a href="{{ route('transactions.index') }}"><button type="button" class="btn btn-lg btn-light mr-3" id="cancel" name="cancel">Cancel</button></a>
            <button type="submit" class="btn btn-lg btn-primary">Save</button>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>
@endsection

@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/simplemde/simplemde.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/simplemde/simplemde.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/editor.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/select.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/select2e.js') }}"></script>
<script type="text/javascript">

jQuery(document).ready(function(){

  jQuery("#add_row").click(function () {
    event.preventDefault();

    var template = `
    <tr>
      <td>
        <select id="category" name="category[]" class="form-control select2">
          @foreach($category as $s)
          <option value="{{ $s->id }}">{{ $s->name }}</option>
          @endforeach
        </select>
      </td>
      <td>
        <div id="effective_date-popup" class="input-group date datepicker">
          <input type="text" class="form-control" id="effective_date" name="effective_date[]" placeholder="dd/mm/yyyy" value="{{ Carbon\Carbon::now()->format('d/m/Y') }}">
          <span class="mp-date-format">dd/mm/yyyy</span>
          <span class="input-group-addon input-group-append border-left">
            <span class="mdi mdi-calendar input-group-text"></span>
          </span>
        </div>
      </td>
      <td>
        <input type="text" class="form-control" id="name" name="name[]" placeholder="desc" value="">
      </td>
      <td>
        <input type="number" class="form-control" id="amount" name="amount[]" placeholder="amount" value="">
      </td>
      <td>
        <a href="#" class="text-danger mr-1 p-2" id="remove_row"><i class="mdi mdi-delete"></i></a>
      </td>
    </tr>
    `;
    jQuery('#mp_table > tbody:last-child').append(template);

    jQuery('.datepicker').each(function(){
      jQuery(this).datepicker({
        format: DATE_FORMAT_DATEPICKER,
        todayBtn: true,
        clearBtn: true,
        autoclose: true,
        todayHighlight: true,
        forceParse: false,
      });
    });

    jQuery('select.select2e').each(function(){
      var input = jQuery(this);      
      var route = input.attr('select2_route');
      var token = jQuery("input[name='_token']").val();

      input.select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: '',
        ajax: {
          dataType: 'json',
          type: "POST",
          url: route,
          data: function(params) {
            return {
              search: params.term,
              _token: token,
            }
          },
          processResults: function (data, page) {
            return {
              results: data
            };
          },
        }
      });

    });
  });  

  jQuery("#mp_table").on('click', '#remove_row', function () {
    jQuery(this).closest('tr').remove();
  });

  jQuery('#mpform').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) { 
      e.preventDefault();
      return false;
    }
  });

});

</script>
@endsection
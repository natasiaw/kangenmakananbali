@extends('template.main')
@section('main-content')
<meta name="_token" content="{{ csrf_token() }}">
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Transactions</h4>
      </div>
    </div>
  </div>
</div>

@include('template.error')

<div class="row">
  <div class="col-lg-12 col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">   
        <p>
          <a href="{{ route('transactions.create') }}">
            <button class="btn btn-lg btn-primary mb-3" id="delete" name="delete">Create New</button>
          </a>
        </p>

        <div class="form-group row ml-0 mb-0">
          <label for="report_date" class="col-sm-2 col-form-label">Date</label>
          <div class="col-sm-3">
            <div id="from_date-popup" class="input-group date datepicker">
              <input type="text" class="form-control" id="from_date" name="from_date" placeholder="dd/mm/yyyy" value="{{ Carbon\Carbon::createFromTimestamp( $fromDate )->format('d/m/Y') }}">
              <span class="mp-date-format">dd/mm/yyyy</span>
              <span class="input-group-addon input-group-append border-left">
                <span class="mdi mdi-calendar input-group-text"></span>
              </span>                   
            </div>
          </div>          

          <label for="report_date" class="col-sm-2 col-form-label">Date</label>
          <div class="col-sm-3">
            <div id="to_date-popup" class="input-group date datepicker">
              <input type="text" class="form-control" id="to_date" name="to_date" placeholder="dd/mm/yyyy" value="{{ Carbon\Carbon::now()->format('d/m/Y') }}">
              <span class="mp-date-format">dd/mm/yyyy</span>
              <span class="input-group-addon input-group-append border-left">
                <span class="mdi mdi-calendar input-group-text"></span>
              </span>                   
            </div>
          </div>    
        </div>    

        <div class="row">
          <div class="col-12">
            <div class="table-responsive-lg">
              <table id="table-listing" class="table table-bordered table-hover">
                <thead class="thead-dark">
                  <tr>
                    <th>&nbsp;</th> 
                    <th>Category</th>
                    <th>Date</th>
                    <th>Descriptions</th>
                    <th>Amount</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/sweetalert/sweetalert.min.js') }}"></script>
<script type="text/javascript">
  var url_base      = "{{ route('transactions.index') }}";  
  var data_route    = "{{ route('transactions.getdata') }}";
</script>
<script type="text/javascript">
$(document).ready(function() {
  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  var t = $('#table-listing').DataTable({
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      ordering: true,
      order: [1, 'asc'],
      pageLength: 50,
      ajax: {
          type: "GET",
          url: data_route,
          data: function ( d ) {
            d.from_date = jQuery('#from_date').val();
            d.to_date = jQuery('#to_date').val();
          },
          dataType: "json",
          'error': function (xhr, textStatus, ThrownException) {
            //console.log(xhr.responseText);
          }
      },
      columns: [
        {
          data: "id", 
          defaultContent: '',
          searchable: false,
          orderable: false,
        },
        { 
          data: "category",
          render: function(data, type, row) {
            switch (data.category_type.is_expenses) 
            {
              case 0:
                return '<label class="badge badge-info mr-2">'+data.category_type.name+'</label> ' + data.name;
              case 1:
                return '<label class="badge badge-danger mr-2">'+data.category_type.name+'</label>' + data.name;
            }
           
          }
        },
        { 
          data: "date", 
          render: function (data, type, row) {
            return ( moment.unix( data ).format('DD MMM YYYY') ) ;
          } 
        },
        { data: "name" },
        { 
          data: "amount",
          render: function(data, type, row) {
            return "<h4>Rp "+ formatNumber(data) + "</h4>";
           
          }
        },
        {
          data: 'id',
          render: function(data, type, row) {
            var string_button = "";
           
            string_button += '<a href="' + url_base + "/" + data + '/edit" class="text-primary mr-1 p-2"><i class="mdi mdi-lead-pencil"></i></a>';
            string_button += '<a href="" onclick="showSwal(' + data + ')" class="text-danger mr-1 p-2"><i class="mdi mdi-delete"></i></a>';
            return string_button;
          },
          orderable: false
        }
      ]
    });

    t.on( 'order.dt search.dt', function () {
      t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          t.cell(cell).invalidate('dom');
      } );
    }).draw();

    jQuery('#from_date').change(function(){
      t.ajax.reload();
    });  

    jQuery('#to_date').change(function(){
      t.ajax.reload();
    });  

    $.ajaxSetup({
      headers: {
        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
      }
    });
    
    showSwal = function(item_id) {
      event.preventDefault();

      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        buttons: {
          cancel: {
            text: "Cancel",
            value: null,
            visible: true,
            closeModal: true,
          },
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            closeModal: true,
          }
        }
      }).then((result) => {
        if (result) {
          var token = $('[name="_token"]').val();
          $.post(url_base + '/' + item_id, {"_method" : "DELETE", "_token" : token}, function(response) {
            if (response == true) {
              swal({
                title: 'Deleted!',
                text: 'Item has been deleted.',
                icon: 'success'
              }).then((result) => {
                window.location.reload();
              });    
            } else {
              swal({
                title: 'Delete failed!',
                text: 'Item is not deleted.',
                icon: 'error'
              })
            }
            
          });
        } 
      })
    };

});
</script>
@endsection

@extends('master.advanced-1')

@section('title')
Province
@endsection

@section('parent-name')
Country
@endsection

@section('button-create')
{{ route('province.create') }}
@endsection

@section('js-content-basic')
<script type="text/javascript">
  var url_base      = "{{ route('province.index') }}";  
  var data_route    = "{{ route('province.getdata') }}";
</script>
<script type="text/javascript" src="{{ asset('/js/master/province.datatables.js') }}"></script>
@endsection
@extends('master.basic')

@section('title')
Order Status
@endsection

@section('button-create')
{{ route('orderStatus.create') }}
@endsection

@section('js-content-basic')
<script type="text/javascript">
  var url_base      = "{{ route('orderStatus.index') }}";  
  var data_route    = "{{ route('orderStatus.getdata') }}";
</script>
@endsection

@extends('template.main')
@section('main-content')
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Order Status</h4>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-6 col-md-12 d-flex flex-column">
  
  @include('template.error')
  
    <div class="card">
      <div class="card-body">
        
        <form class="mpform" id="mpform" method="POST" action="@if( isset($orderStatus->id) ){{ route('orderStatus.update', ['orderStatus' => $orderStatus->id]) }}@else{{ route('orderStatus.store') }}@endif">
          @if( isset($orderStatus->id) ) {{ method_field('PATCH') }} @else {{ method_field('POST') }} @endif          
          @csrf
          <input type="hidden" id="id" name="id" value="{{ $orderStatus->id }}">
          <fieldset class="form-group">
            <div class="row">
              <legend class="col-form-label col-sm-3 pt-0">Status</legend>
              <div class="col-sm-9">
                <div class="form-check form-check-inline">
                  <label for="status" class="form-check-label">
                  <input type="checkbox" class="form-check-input" id="status" name="status" 
                      @if( isset($orderStatus->id) ) @if( $orderStatus->status == 1 ) checked @else unchecked @endif @else checked @endif
                    />
                    Status
                  <i class="input-helper"></i></label>
                </div>
              </div>
            </div>
          </fieldset> 
          <div class="form-group row">
            <label for="name" class="col-sm-3 col-form-label">Name</label>
            <div class="col-sm-9">
              <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" placeholder="Name" value="{{ $errors->has('name') ? old('name' ) : $orderStatus->name }}">
            </div>
          </div>
          <div class="form-group row">
            <label for="note" class="col-sm-3 col-form-label">Description</label>
            <div class="col-sm-9">
              <textarea name="note" id="note" class="simpleMde">{!! $orderStatus->note !!}</textarea>
            </div>
          </div>  
          
          <div class="form-group mt-5 float-right">             
            <a href="{{ route('orderStatus.index') }}"><button type="button" class="btn btn-lg btn-light mr-3" id="cancel" name="cancel">Cancel</button></a>
            <button type="submit" class="btn btn-lg btn-primary">Save</button>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>
@endsection

@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/simplemde/simplemde.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/simplemde/simplemde.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/editor.js') }}"></script>
@endsection
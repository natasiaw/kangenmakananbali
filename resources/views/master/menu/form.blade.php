@extends('template.main')
@section('main-content')
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Menu</h4>
      </div>
    </div>
  </div>
</div>

<form class="mpform" id="mpform" method="POST" action="@if( isset($model->id) ){{ route('menu.update', ['menu' => $model->id]) }}@else{{ route('menu.store') }}@endif">
  @if( isset($model->id) ) {{ method_field('PATCH') }} @else {{ method_field('POST') }} @endif          
  @csrf

  <input type="hidden" id="id" name="id" value="">

  @include('template.error')

<div class="row">
  <div class="col-lg-6 d-flex flex-column">
    <div class="row">

      <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">       
            <fieldset class="form-group">
              <div class="row">
                <legend class="col-form-label col-sm-3 pt-0">Status</legend>
                <div class="col-sm-9">
                  <div class="form-check form-check-inline">
                    <label for="status" class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="status" name="status" 
                        @if( isset($model->id) ) @if( $model->status == 1 ) checked @else unchecked @endif @else checked @endif
                      />
                      Status
                    <i class="input-helper"></i></label>
                  </div>
                </div>
              </div>
            </fieldset> 
            <div class="form-group row">
              <label for="restaurant" class="col-sm-3 col-form-label">Restaurant</label>
              <div class="col-sm-9">
                <select name="restaurant" class="form-control select2">
                  @foreach($restaurant as $s)
                  <option value="{{ $s->id }}" @if($model->m_restaurant_id == $s->id) selected @endif>{{ $s->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="name" class="col-sm-3 col-form-label">Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" placeholder="Name" value="{{ $errors->has('name') ? old('name' ) : $model->name }}">
              </div>
            </div>
            <div class="form-group row">
              <label for="price_original" class="col-sm-3 col-form-label">Restaurant Price</label>
              <div class="col-sm-9">
                <input type="text" class="form-control {{ $errors->has('price_original') ? 'is-invalid' : '' }}" id="price_original" name="price_original" placeholder="Price Original" value="{{ $errors->has('price_original') ? old('price_original' ) : $model->original_price }}">
              </div>
            </div>
            <div class="form-group row">
              <label for="online_price" class="col-sm-3 col-form-label">Online Price</label>
              <div class="col-sm-9">
                <input type="text" class="form-control {{ $errors->has('online_price') ? 'is-invalid' : '' }}" id="online_price" name="online_price" placeholder="KMB Original" value="{{ $errors->has('online_price') ? old('online_price' ) : $model->online_price }}">
              </div>
            </div>
            <div class="form-group row">
              <label for="kmb_price" class="col-sm-3 col-form-label">KMB Price</label>
              <div class="col-sm-9">
                <input type="text" class="form-control {{ $errors->has('kmb_price') ? 'is-invalid' : '' }}" id="kmb_price" name="kmb_price" placeholder="KMB Original" value="{{ $errors->has('kmb_price') ? old('kmb_price' ) : $model->kmb_price }}">
              </div>
            </div>
            <fieldset class="form-group">
              <div class="row">
                <legend class="col-form-label col-sm-3 pt-0">PO Status</legend>
                <div class="col-sm-9">
                  <div class="form-check form-check-inline">
                    <label for="po_status" class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="po_status" name="po_status" 
                        @if( isset($model->id) ) @if( $model->po_status == 1 ) checked @else unchecked @endif @else checked @endif
                      />
                      PO Status
                    <i class="input-helper"></i></label>
                  </div>
                </div>
              </div>
            </fieldset> 
            <div class="form-group row">
              <label for="note" class="col-sm-3 col-form-label">Description</label>
              <div class="col-sm-9">
                <textarea name="note" id="note" class="simpleMde">{!! $model->note !!}</textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 d-flex flex-column">

    <div class="card">
      <div class="card-body">          
        <div class="form-group float-right mb-0">        
          <a href="{{ URL::previous() }}"><button type="button" class="btn btn-lg btn-light mr-3" id="cancel" name="cancel">Cancel</button></a>     
          <button type="submit" class="btn btn-lg btn-primary">Save</button>
        </div>
      </div>
    </div>

  </div>
</div> 

</form>
@endsection

@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/simplemde/simplemde.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/simplemde/simplemde.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/select.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/select2e.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/editor.js') }}"></script>
@endsection
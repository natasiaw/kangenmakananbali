@extends('template.main')
@section('main-content')
<meta name="_token" content="{{ csrf_token() }}">
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Price List</h4>
      </div>
    </div>
  </div>
</div>

@include('template.error')

<div class="row">
  <div class="col-lg-6 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Kangen Makanan Bali</h4>
        <div class="table-responsive">
          <table id="table-listing" class="table table-bordered table-hover">
            <thead class="thead-dark">
              <tr>
                <th>&nbsp;</th> 
                <th>&nbsp;</th> 
                <th style="width:20%">Menu</th>
                <th style="width:20%">Price</th>
                <th style="width:20%">Note</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {
    function formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    var data_route    = "{{ route('menu.get_release') }}";

		var groupColumn = 1;
			var t = jQuery('#table-listing').DataTable({
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			dom: 'Bfrtip',
			pageLength: 15,
			ajax: {
				type: "GET",
				url: data_route,
				dataType: "json",
				'error': function (xhr, textStatus, ThrownException) {
				console.log(xhr);
				}
			},
			columns: [
				{
					data: "id", 
					defaultContent: '',
					searchable: false,
					orderable: false,
					visible:false,
				},
				{ 
					data: "restaurant.name", 
					visible:false,
				},
				{ data: "name" },
				
				{
					data: 'kmb_price',
					render: function(data, type, row) {
					if(data != null){
					return "Rp "+ formatNumber(data);
					}else{
					return "";
					}
					
				}
				
				},
				{ data: "note" },
			],
			"drawCallback": function ( settings ) {
				var api = this.api();
				var rows = api.rows( {page:'current'} ).nodes();
				var last=null;

				api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
				group = group;
					if ( last !== group ) {
						$(rows).eq( i ).before(
							'<tr class="group"><td colspan="10"><label class="badge badge-primary mr-2">'+group+'</label></td></tr>'
						);

						last = group;
					}
				} );
			},
			
		         "order": [[1,'asc']]
			});

			t.on('draw.dt', function () {
				var info = t.page.info();
				t.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
					cell.innerHTML = i + 1 + info.start;
				});
			});
  });
</script>	
@endsection

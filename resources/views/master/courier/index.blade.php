@extends('master.basic')

@section('title')
Courier
@endsection

@section('button-create')
{{ route('courier.create') }}
@endsection

@section('js-content-basic')
<script type="text/javascript">
  var url_base      = "{{ route('courier.index') }}";  
  var data_route    = "{{ route('courier.getdata') }}";
</script>
@endsection

@extends('template.main')
@section('main-content')
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Customer</h4>
      </div>
    </div>
  </div>
</div>

<form class="mpform" id="mpform" method="POST" action="@if( isset($model->id) ){{ route('customer.update', ['customer' => $model->id]) }}@else{{ route('customer.store') }}@endif">
  @if( isset($model->id) ) {{ method_field('PATCH') }} @else {{ method_field('POST') }} @endif          
  @csrf

  <input type="hidden" id="id" name="id" value="{{$model->id}}">

  @include('template.error')

<div class="row">
  <div class="col-lg-6 d-flex flex-column">
    <div class="row">

      <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">       
            <h4 class="card-title mb-5">Personal Information</h4>
            <fieldset class="form-group">
              <div class="row">
                <legend class="col-form-label col-sm-3 pt-0">Status</legend>
                <div class="col-sm-9">
                  <div class="form-check form-check-inline">
                    <label for="status" class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="status" name="status" 
                        @if( isset($model->id) ) @if( $model->status == 1 ) checked @else unchecked @endif @else checked @endif
                      />
                      Status
                    <i class="input-helper"></i></label>
                  </div>
                </div>
              </div>
            </fieldset> 
            <div class="form-group row">
              <label for="full_name" class="col-sm-3 col-form-label">Full Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control {{ $errors->has('full_name') ? 'is-invalid' : '' }}" id="full_name" name="full_name" placeholder="Full Name" value="{{ $errors->has('name') ? old('name' ) : $model->name }}">
              </div>
            </div>
            <div class="form-group row">
              <label for="address" class="col-sm-3 col-form-label">Address</label>
              <div class="col-sm-9">
                <input type="text" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" id="address" name="address" placeholder="Address" value="{{ $errors->has('address') ? old('address' ) : $model->address }}">
              </div>
            </div>
            <div class="form-group row">
              <label for="postal_code" class="col-sm-3 col-form-label">Subdivision</label>
              <div class="col-sm-9">
                <select id="postal_code" name="postal_code" class="form-control select2e" select2_route="{{ route('postalCode.getdataforselect') }}">
                 
                </select> 
              </div>
            </div>
            <div class="form-group row">
              <label for="full_name" class="col-sm-3 col-form-label">Phone</label>
              <div class="col-sm-9">
                <input type="text" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" id="phone" name="phone" placeholder="Phone" value="{{ $errors->has('phone') ? old('phone' ) : $model->phone }}">
              </div>
            </div>
            <div class="form-group row">
              <label for="note" class="col-sm-3 col-form-label">Description</label>
              <div class="col-sm-9">
                <textarea name="note" id="note" class="simpleMde">{!! $model->note !!}</textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 d-flex flex-column">

    <div class="card">
      <div class="card-body">          
        <div class="form-group float-right mb-0">        
          <a href="{{ URL::previous() }}"><button type="button" class="btn btn-lg btn-light mr-3" id="cancel" name="cancel">Cancel</button></a>     
          <button type="submit" class="btn btn-lg btn-primary">Save</button>
        </div>
      </div>
    </div>

  </div>
</div> 

</form>
@endsection

@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/simplemde/simplemde.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/simplemde/simplemde.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/select.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/select2e.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/editor.js') }}"></script>
@endsection
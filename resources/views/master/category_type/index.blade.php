@extends('template.main')
@section('main-content')
<meta name="_token" content="{{ csrf_token() }}">
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Category Type</h4>
      </div>
    </div>
  </div>
</div>

@include('template.error')

<div class="row">
  <div class="col-lg-12 col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">   
        <p>
          <a href="{{ route('categoryType.create') }}">
            <button class="btn btn-lg btn-primary mb-3" id="delete" name="delete">Create New</button>
          </a>
        </p>
        <div class="row">
          <div class="col-12">
            <div class="table-responsive-lg">
              <table id="table-listing" class="table table-bordered table-hover">
                <thead class="thead-dark">
                  <tr>
                    <th>&nbsp;</th> 
                    <th>Name</th>
                    <th>Is Expenses</th>
                    <th>Descriptions</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/moment/moment.min.js') }}"></script>
<script type="text/javascript">
  var url_base      = "{{ route('categoryType.index') }}";  
  var data_route    = "{{ route('categoryType.getdata') }}";
</script>
<script type="text/javascript">
$(document).ready(function() {

  var t = $('#table-listing').DataTable({
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      dom: 'Bfrtip',
      buttons: [
          'pageLength', 
          {
            extend: 'copy',
            exportOptions: { 
              columns: [ 0, 1, 2 ], 
              rows: ':visible' 
            }
          },
          {
            extend: 'csv',
            exportOptions: { 
              columns: [ 0, 1, 2 ], 
              rows: ':visible' 
            }
          },
          {
            extend: 'excel',
            exportOptions: { 
              columns: [ 0, 1, 2 ], 
              rows: ':visible' 
            }
          },
          {
            extend: 'pdf',
            exportOptions: { 
              columns: [ 0, 1, 2 ], 
              rows: ':visible' 
            },
            customize: function (doc) {
              doc.content[1].table.widths = 
                  Array(doc.content[1].table.body[0].length + 1).join('*').split('');
            }
          },
          {
            extend: 'print',
            exportOptions: { 
              columns: [ 0, 1, 2 ], 
              rows: ':visible' 
            }
          }
      ],
      ordering: true,
      order: [1, 'asc'],
      pageLength: 50,
      ajax: {
          type: "GET",
          url: data_route,
          dataType: "json",
          'error': function (xhr, textStatus, ThrownException) {
            console.log(ThrownException);
            // alert('Error loading data. Exception: ' + ThrownException + "\n" + textStatus);
          }
      },
      columns: [
        {
          data: "id", 
          defaultContent: '',
          searchable: false,
          orderable: false,
        },
        { data: "name" },
        { 
          data: "is_expenses", 
          render: function (data, type, row) {
            switch (data) 
            {
              case 0:
                return '<label class="badge badge-info mr-2">No</label>';
              case 1:
                return '<label class="badge badge-danger mr-2">Yes</label>';
            }
          } 
        },
        { data: "note" },
        {
          data: 'id',
          render: function(data, type, row) {
            var string_button = "";
           
            string_button += '<a href="' + url_base + "/" + data + '/edit" class="text-primary mr-1 p-2"><i class="mdi mdi-lead-pencil"></i></a>';
            string_button += '<a href="" onclick="showSwal(' + data + ')" class="text-danger mr-1 p-2"><i class="mdi mdi-delete"></i></a>';
            return string_button;
          },
          orderable: false
        }
      ]
    });

    t.on( 'order.dt search.dt', function () {
      t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
          cell.innerHTML = i+1;
          t.cell(cell).invalidate('dom');
      } );
    }).draw();

    $.ajaxSetup({
      headers: {
        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
      }
    });
    
    showSwal = function(item_id) {
      event.preventDefault();

      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        buttons: {
          cancel: {
            text: "Cancel",
            value: null,
            visible: true,
            closeModal: true,
          },
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            closeModal: true,
          }
        }
      }).then((result) => {
        if (result) {
          var token = $('[name="_token"]').val();
          $.post(url_base + '/' + item_id, {"_method" : "DELETE", "_token" : token}, function(response) {
            if (response == true) {
              swal({
                title: 'Deleted!',
                text: 'Item has been deleted.',
                icon: 'success'
              }).then((result) => {
                window.location.reload();
              });    
            } else {
              swal({
                title: 'Delete failed!',
                text: 'Item is not deleted.',
                icon: 'error'
              })
            }
            
          });
        } 
      })
    };

});
</script>
@endsection

@extends('template.main')
@section('main-content')
<meta name="_token" content="{{ csrf_token() }}">
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Master Postal Code</h4>
      </div>
    </div>
  </div>
</div>

@include('template.error')

<div class="row">
  <div class="col-lg-12 col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <div class="form-group row">
              <label for="province" class="col-sm-2 col-form-label">Province</label>
              <div class="col-sm-5">
                <select id="province" name="province" class="form-control select2">
                  <option value="-1">ALL</option>
                  @foreach($provinces as $s)
                  <option value="{{ $s->id }}">{{ $s->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>  
            <div class="table-responsive-lg">
              <table id="table-listing" class="table table-bordered table-hover">
                <thead class="thead-dark">
                  <tr>
                    <th>&nbsp;</th>
                    <th>Province</th>
                    <th>Regency</th>
                    <th>District</th>
                    <th>Village</th>
                    <th>Postal Code</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/sweetalert/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/moment/moment.min.js') }}"></script>
<script type="text/javascript">
  var url_base      = "{{ route('postalCode.index') }}";  
  var data_route    = "{{ route('postalCode.getdata') }}";
</script>
<script type="text/javascript" src="{{ asset('/js/master/postalCode.datatables.js') }}"></script>
@endsection

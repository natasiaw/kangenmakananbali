@extends('template.main')
@section('main-content')
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Master Leave Period</h4>
      </div>
    </div>
  </div>
</div>

@include('template.error')

<form class="mpform" id="mpform" method="POST" action="@if( isset($model->id) ){{ route('leavePeriod.update', ['leavePeriod' => $model->id]) }}@else{{ route('leavePeriod.store') }}@endif">
  @if( isset($model->id) ) {{ method_field('PATCH') }} @else {{ method_field('POST') }} @endif          
  @csrf
  <input type="hidden" id="id" name="id" value="{{ $model->id }}"> 
  
<div class="row">
  <div class="col-lg-6 col-md-12 d-flex flex-column grid-margin">
    <div class="card">
      <div class="card-body">        
        <h4 class="mb-3">Period Details</h4>
        
        <div class="form-group row">
          <label for="name" class="col-sm-3 col-form-label">Name</label>
          <div class="col-sm-9">
            <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" id="name" name="name" placeholder="Name" value="{{ $errors->has('name') ? old('name' ) : $model->name }}">
          </div>
        </div>
        <div class="form-group row">
          <label for="username" class="col-sm-3 col-form-label">Start Date</label>
          <div class="col-sm-9">
            <div id="start_date-popup" class="input-group date datepicker">
              <input type="text" class="form-control" id="start_date" name="start_date" placeholder="dd/mm/yyyy" value="@if( isset($model->start_date) ) {{ Carbon\Carbon::createFromTimestamp( $model->start_date ) }} @else {{ Carbon\Carbon::now() }} @endif">
              <span class="mp-date-format">dd/mm/yyyy</span>
              <span class="input-group-addon input-group-append border-left">
                <span class="mdi mdi-calendar input-group-text"></span>
              </span>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="username" class="col-sm-3 col-form-label">End Date</label>
          <div class="col-sm-9">
            <div id="end_date-popup" class="input-group date datepicker">
              <input type="text" class="form-control" id="end_date" name="end_date" placeholder="dd/mm/yyyy" value="@if( isset($model->end_date) ) {{ Carbon\Carbon::createFromTimestamp( $model->end_date ) }} @else {{ Carbon\Carbon::now() }} @endif">
              <span class="mp-date-format">dd/mm/yyyy</span>
              <span class="input-group-addon input-group-append border-left">
                <span class="mdi mdi-calendar input-group-text"></span>
              </span>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="note" class="col-sm-3 col-form-label">Description</label>
          <div class="col-sm-9">
            <textarea name="note" id="note" class="simpleMde">{!! $model->note !!}</textarea>
          </div>
        </div> 
        
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin">
    <div class="card">
      <div class="card-body">    

        <div class="d-flex flex-wrap justify-content-between">
          <h4 class="card-title">Leave Rule</h4>
          <button type="button" class="btn btn-primary btn-sm btn-icon-text" id="rule_add">
            <i class="mdi mdi-plus-box btn-icon-prepend"></i>
            Add
          </button>
        </div>
        <div class="table-responsive-sm">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>Group</th>
                <th>Type</th>
                <th>Max Days</th>
                <th>Max Cons. Days</th>
                <th width="5%">&nbsp;</th>
              </tr>
            </thead>
            <tbody id="rule_table">
            @if( isset($model->id) )
              @foreach($leave_rule as $lr)
                <tr>
                  <td>
                    <input type="hidden" name="rule_group[]" id="rule_group" value="{{ $lr->id_m_leave_group }}">
                    {{ $lr->leave_group->name }}
                  </td>
                  <td>
                    <input type="hidden" name="rule_type[]" id="rule_group" value="{{ $lr->id_m_leave_type }}">
                    {{ $lr->leave_type->name }}
                  </td>
                  <td>
                    <input type="number" name="rule_days[]" id="rule_days" class="form-control" value="{{ $lr->days }}" min="0" max="99">
                  </td>
                  <td>
                    <input type="number" name="rule_maxdays[]" id="rule_maxdays" class="form-control" value="{{ $lr->max_days }}" min="0" max="99">
                  </td>
                  <td align="center">
                    &nbsp;
                  </td>
                </tr>
              @endforeach
            @else
              @foreach($leave_group as $lg)
              @foreach($leave_type as $lt)
                <tr>
                  <td>
                    <input type="hidden" name="rule_group[]" id="rule_group" value="{{ $lg->id }}">
                    {{ $lg->name }}
                  </td>
                  <td>
                    <input type="hidden" name="rule_type[]" id="rule_group" value="{{ $lt->id }}">
                    {{ $lt->name }}
                  </td>
                  <td>
                    <input type="number" name="rule_days[]" id="rule_days" class="form-control" value="0" min="0" max="99">
                  </td>
                  <td>
                    <input type="number" name="rule_maxdays[]" id="rule_maxdays" class="form-control" value="0" min="0" max="99">
                  </td>
                  <td align="center">
                    <a class="mr-1 text-danger p-2 detail_delete"><i class="mdi mdi-delete"></i></a>
                  </td>
                </tr>
              @endforeach
              @endforeach
            @endif            
            </tbody>
          </table>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12 d-flex flex-column">
    <div class="card">
      <div class="card-body">    
        <div class="form-group float-right mb-0">             
          <a href="{{ route('leavePeriod.index') }}"><button type="button" class="btn btn-lg btn-light mr-3" id="cancel" name="cancel">Cancel</button></a>
          <button type="submit" class="btn btn-lg btn-primary">Save</button>
        </div>
      </div>
    </div>
  </div>
</div>

</form>

@endsection

@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/simplemde/simplemde.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/simplemde/simplemde.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/editor.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
  
  jQuery('#rule_table').on('input', '#rule_days', function(){
    var val = jQuery(this).val();
    jQuery(this).parents("tr").find('#rule_maxdays').val(val);
    jQuery(this).parents("tr").find('#rule_maxdays').attr('max', val);
  });

  jQuery('#rule_add').click(function(){
    event.preventDefault();
    
    tr = (jQuery('<tr/>'));
    var template = `
      <td>
        <select name="rule_group[]" class="form-control select2">
          @foreach($leave_group as $s)
          <option value="{{ $s->id }}">{{ $s->name }}</option>
          @endforeach
        </select>
      </td>
      <td>
        <select name="rule_type[]" class="form-control select2">
          @foreach($leave_type as $s)
          <option value="{{ $s->id }}">{{ $s->name }}</option>
          @endforeach
        </select>
      </td>
      <td>
        <input type="number" name="rule_days[]" id="rule_days" class="form-control" value="" min="0" max="99">
      </td>
      <td>
        <input type="number" name="rule_maxdays[]" id="rule_maxdays" class="form-control" value="" min="0" max="99">
      </td>
      <td align="center">
        <a class="mr-1 text-danger p-2 detail_delete"><i class="mdi mdi-delete"></i></a>
      </td>
    `;

    tr.append(template);
    tr.appendTo('#rule_table');
  });
  
  jQuery('#rule_table').on('click', '.detail_delete', function(){
    event.preventDefault();
    jQuery(this).parents("tr").remove();
  });

});
</script>
@endsection
@extends('master.basic')

@section('title')
Country
@endsection

@section('button-create')
{{ route('country.create') }}
@endsection

@section('js-content-basic')
<script type="text/javascript">
  var url_base      = "{{ route('country.index') }}";  
  var data_route    = "{{ route('country.getdata') }}";
</script>
@endsection

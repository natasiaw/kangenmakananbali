@extends('template.main')
@section('main-content')
<meta name="_token" content="{{ csrf_token() }}">
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Order</h4>
      </div>
    </div>
  </div>
</div>

@include('template.error')

<div class="row">
  <div class="col-lg-12 col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">   
        <p>
          <a href="{{ route('order.create') }}">
            <button class="btn btn-lg btn-primary mb-3" id="delete" name="delete">Create New</button>
          </a>
        </p>
        <div class="form-group row ml-0 mb-0">
          <label for="orderStatus" class="col-sm-2 col-form-label">Order Status</label>
            <div class="col-sm-4">
              <select id="orderStatus" name="orderStatus" class="form-control select2">
                <option value="-1">ALL except Complete</option>
                @foreach($orderStatus as $s)
                <option value="{{ $s->id }}">{{ $s->name }}</option>
                @endforeach
              </select>
            </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="table-responsive-lg">
              <table id="table-listing" class="table table-bordered table-hover">
                <thead class="thead-dark">
                  <tr>
                    <th>&nbsp;</th> 
                    <th>Customer</th>
                    <th>Menu Order</th>
                    <th>Bill</th>
                    <th>Profit</th>
                    <th>Point</th>
                    <th>Note</th>
                    <th>Status</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/sweetalert/sweetalert.min.js') }}"></script>
<script type="text/javascript">
  var url_base      = "{{ route('order.index') }}";  
  var data_route    = "{{ route('order.getdata') }}";
</script>
<script type="text/javascript">

$(document).ready(function() {

  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  var t = $('#table-listing').DataTable({
      bFilter: false,
      processing: true,
      serverSide: true,
      destroy: true,
      lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      ordering: true,
      order: [7, 'asc'],
      pageLength: 50,
      ajax: {
          type: "GET",
          url: data_route,
          data: function ( d ) {
            d.order_status = jQuery('#orderStatus').val();
          },
          dataType: "json",
          'error': function (xhr, textStatus, ThrownException) {
            console.log(xhr);
            // alert('Error loading data. Exception: ' + ThrownException + "\n" + textStatus);
          }
      },
      columns: [
        {
          data: "id", 
          defaultContent: '',
          searchable: false,
          orderable: false,
        },
        { 
          data: "customer",
          render: function(data, type, row) {
            var str = "<strong>"+data.name +"</strong><br>"+ data.phone +"<br>"+ data.address;
            str += "<br>" + data.postal_code.village +", "+ data.postal_code.district +", "+ data.postal_code.regency;
            str += "<br>" + data.postal_code.province.name +", "+ data.postal_code.postal_code;
            
            return str;
          }
        },
        { 
          data: "order_detail",
          render: function(data, type, row) {
            var str = "";
            data.forEach(function (item){
              str += item.menu.restaurant.name+" - <strong>"+item.menu.name +"</strong> : "+ item.quantity + "<br>";
            });
            return str;
          }
        },
        { 
          data: "order_detail",
          render: function(data, type, row) {
            var total_price = 0;
            data.forEach(function (item){
              total_price += Number((item.kmb_price*item.quantity));
            });

            total_price +=(row.shipping_cost*row.weight) - row.promo.price;
           
            return "<h4>Rp "+ formatNumber(total_price) + "</h4>";
           
          }
        },
        { 
          data: "order_detail",
          render: function(data, type, row) {
            var total_price = 0;
            data.forEach(function (item){
              total_price += Number((item.kmb_price*item.quantity)-(item.original_price*item.quantity));
            });
            total_price += Number(-row.additional_cost);
            return "<h4>Rp "+ formatNumber(total_price) + "</h4>";
           
          }
        },
        { 
          data: "order_detail",
          render: function(data, type, row) {
            var total_price = 0;
            data.forEach(function (item){
              total_price += Number((item.kmb_price*item.quantity)/1000);
            });
            if(total_price % 1 == 0){
              return "<h4> "+ formatNumber(total_price) + "</h4>"; 
            }else{
              return "<h4> "+ Math.floor(formatNumber(total_price)) + "</h4>";
            }
           
          }
        },
        { 
          data: "order_detail",
          render: function(data, type, row) {
            var str="";
            data.forEach(function (item){
              if(item.note != null){
                str += item.note +"</br>";
              }
            });
            return str;
           
          }
        },
        {
          data: 'order_status',
          render: function(data, type, row) {
            switch (data.id) 
            {
              case 0:
                return '<label class="badge badge-warning mr-2">'+data.name+'</label>';
              case 1:
                return '<label class="badge badge-success mr-2">'+data.name+'</label>';
              case 2:
                return '<label class="badge badge-primary mr-2">'+data.name+'</label>';
              case 3:
                return '<label class="badge badge-info mr-2">'+data.name+'</label>';
              case 4:
                return '<label class="badge badge-danger mr-2">'+data.name+'</label>';
              default:
                return '<label class="badge badge-danger mr-2">'+data.name+'</label>';
            }
            
          }
        },
        {
          data: 'id',
          render: function(data, type, row) {
            var string_button = "";
           
            string_button += '<a href="' + url_base + "/" + data + '" class="text-warning mr-1 p-2"><i class="mdi mdi-eye"></i></a>';
            string_button += '<a href="' + url_base + "/" + data + '/edit" class="text-primary mr-1 p-2"><i class="mdi mdi-lead-pencil"></i></a>';
            string_button += '<a href="" onclick="showSwal(' + data + ')" class="text-danger mr-1 p-2"><i class="mdi mdi-delete"></i></a>';
            return string_button;
          },
          orderable: false
        }
      ]
    });

    t.on('draw.dt', function () {
      var info = t.page.info();
      t.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
        cell.innerHTML = i + 1 + info.start;
      });
    });

    jQuery('#orderStatus').change(function(){
      t.ajax.reload();
    }); 

    $.ajaxSetup({
      headers: {
        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
      }
    });
    
    showSwal = function(item_id) {
      console.log(item_id);
      event.preventDefault();

      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        buttons: {
          cancel: {
            text: "Cancel",
            value: null,
            visible: true,
            closeModal: true,
          },
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            closeModal: true,
          }
        }
      }).then((result) => {
        if (result) {
          var token = $('[name="_token"]').val();
          $.post(url_base + '/' + item_id, {"_method" : "DELETE", "_token" : token}, function(response) {
            if (response == true) {
              swal({
                title: 'Deleted!',
                text: 'Item has been deleted.',
                icon: 'success'
              }).then((result) => {
                window.location.reload();
              });    
            } else {
              swal({
                title: 'Delete failed!',
                text: 'Item is not deleted.',
                icon: 'error'
              })
            }
            
          });
        } 
      })
    };

});
</script>
@endsection

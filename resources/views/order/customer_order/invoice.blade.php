@extends('template.main')
@section('main-content')
<meta name="_token" content="{{ csrf_token() }}">
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Order</h4>
      </div>
    </div>
  </div>
</div>

@include('template.error')

<div class="row">
  <div class="col-lg-6 grid-margin stretch-card">
    <div class="card px-2">
        <div class="card-body">
            <div class="container-fluid">
              <h3 class="text-right my-5">Kangen Makanan Bali</h3>
              <hr>
            </div>
            <input type="hidden" id="id" name="id" value="{{ $id }}">
            <div class="container-fluid d-flex justify-content-between">
              <div class="col-lg-3 pl-0"></div>
              <div class="col-lg-6 pr-0">
                <p class="mt-5 mb-2 text-right"><b>Invoice to</b></p>
                <p class="text-right" id="customer_txt"></p>
              </div>
            </div>
            <div class="container-fluid d-flex justify-content-between">
              <div class="col-lg-4 pl-0">
                <p class="mb-0 mt-5">Shipping Date : <span id="ship_date"></span></p>
              </div>
            </div>
            <div class="container-fluid mt-5 d-flex justify-content-center w-100">
              <div class="table-responsive w-100">
                  <table class="table">
                    <thead>
                      <tr class="bg-dark text-white">
                          <th>#</th>
                          <th>Menu</th>
                          <th class="text-right">Quantity</th>
                          <th class="text-right">Unit Price</th>
                          <th class="text-right">Total</th>
                        </tr>
                    </thead>
                    <tbody id="order_list">
                      <tr class="text-right">
                        <td class="text-left">1</td>
                        <td class="text-left">Brochure Design</td>
                        <td>2</td>
                        <td>$20</td>
                        <td>$40</td>
                      </tr>
                      <tr class="text-right">
                        <td class="text-left">2</td>
                        <td class="text-left">Web Design Packages(Template) - Basic</td>
                        <td>05</td>
                        <td>$25</td>
                        <td>$125</td>
                      </tr>
                      <tr class="text-right">
                        <td class="text-left">3</td>
                        <td class="text-left">Print Ad - Basic - Color</td>
                        <td>08</td>
                        <td>$500</td>
                        <td>$4000</td>
                      </tr>
                      <tr class="text-right">
                        <td class="text-left">4</td>
                        <td class="text-left">Down Coat</td>
                        <td>1</td>
                        <td>$5</td>
                        <td>$5</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>
            <div class="container-fluid d-flex justify-content-between">
              <div class="col-lg-4 pl-0">
                <p class="mt-5 mb-2">
                  <b>Bank Transfer:<br>
                  Natasia Wulansari
                  </b>
                </p>
                <p>
                BCA: 860 056 3123<br>
                </p>
              </div>
              <div class="col-lg-3 pr-0">
                <h4 class="mt-5 mb-2 text-right">Total: <span id="tot_price"></span></h4>
              </div>
            </div>
            <div class="container-fluid mt-3 w-100">
                <h5>Notes</h5>
              <ol>
                <li>Harap mengirimkan konfirmasi pembayaran agar pesanan dapat diproses</li>
                <li>Pengiriman menggunakan Paxel dengan eta 2-3 hari karena melalui jalur darat</li>
                <li>No resi akan dikirim selambatnya pada hari Senin</li>
              </ol>
            </div>
        </div>
    </div>
  </div>
</div>
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/moment/moment.min.js') }}"></script>
<script type="text/javascript">
  var url_base      = "{{ route('order.index') }}";  
  var data_route    = "{{ route('order.getdata') }}";
  var invoice_route = "{{ route('order.getinvoice') }}";
</script>
<script type="text/javascript">

jQuery(document).ready(function() {

  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  var token = jQuery("input[name='_token']").val();
  
  jQuery.ajax({
      url: invoice_route,
      method: 'POST',
      data: { id:jQuery("input[name='id']").val(), _token:token },
      success: function(datas) {
        var cust = "<b>"+datas[0].customer.name+"</b><br>"+datas[0].customer.phone+"<br>"+datas[0].customer.address;
        cust +="<br>"+datas[0].customer.postal_code.village +", "+datas[0].customer.postal_code.district+", "+datas[0].customer.postal_code.regency;
        cust +="<br>"+datas[0].customer.postal_code.province.name +", "+datas[0].customer.postal_code.postal_code;
        jQuery("#customer_txt").html(cust);

        var shipping_date = datas[0].actual_shipping_date;
        jQuery("#ship_date").html(moment.unix( shipping_date ).format('DD MMM YYYY'));

        var list_order_detail = "";
        var i=0;
        var total_price = 0;
        datas[0].order_detail.forEach(function (item){
          i += Number(1);
          list_order_detail+="<tr class='text-right'>";
          list_order_detail+="<td class='text-left'>"+ i +"</td>";
          list_order_detail+="<td class='text-left'>"+ item.menu.restaurant.name +" - "+ item.menu.name;
          if(item.note!= null){
            list_order_detail+="<br>"+ item.note+"</td>";
          }else{
            list_order_detail+="</td>";
          }
          

          list_order_detail+="<td>"+item.quantity+"</td>";
          list_order_detail+="<td>Rp "+ formatNumber(item.kmb_price) +"</td>";
          list_order_detail+="<td>Rp "+ formatNumber(item.quantity*item.kmb_price) +"</td>";
          list_order_detail+="</tr>";
          total_price += Number((item.kmb_price*item.quantity));
        });
        total_price +=(datas[0].shipping_cost*datas[0].weight);
        list_order_detail+="<tr class='text-right'>";
        list_order_detail+="<td class='text-left'></td>";
        list_order_detail+="<td class='text-left'>Shipping:"+datas[0].courier.name+"</td>";
        list_order_detail+="<td>"+datas[0].weight+"</td>";
        list_order_detail+="<td>Rp "+ formatNumber(datas[0].shipping_cost) +"</td>";
        list_order_detail+="<td>Rp "+ formatNumber(datas[0].shipping_cost*datas[0].weight) +"</td>";
        list_order_detail+="</tr>";
        if(datas[0].m_promo_id != 4){
          list_order_detail+="<tr class='text-right'>";
          list_order_detail+="<td class='text-left'></td>";
          list_order_detail+="<td class='text-left' colspan='3'>Promo:"+datas[0].promo.name+"</td>";
          list_order_detail+="<td>Rp ("+ formatNumber(datas[0].promo.price) +")</td>";
          list_order_detail+="</tr>";
          total_price -= datas[0].promo.price;
        }

        jQuery("#order_list").html(list_order_detail);
        jQuery("#tot_price").html("Rp "+formatNumber(total_price));

      }
  });


});
</script>
@endsection

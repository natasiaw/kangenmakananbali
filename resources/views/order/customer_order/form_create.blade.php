@extends('template.main')
@section('main-content')
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Order</h4>
      </div>
    </div>
  </div>
</div>

<form class="mpform" id="mpform" method="POST" action="@if( isset($model->id) ){{ route('order.update', ['order' => $model->id]) }}@else{{ route('order.store') }}@endif">
  @if( isset($model->id) ) {{ method_field('PATCH') }} @else {{ method_field('POST') }} @endif          
  @csrf

  <input type="hidden" id="id" name="id" value="">

  @include('template.error')

<div class="row">
  <div class="col-lg-6 d-flex flex-column">
    <div class="row">
      <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">    
            <h4 class="card-title mb-5">Order Information</h4>   
            <fieldset class="form-group">
              <div class="row">
                <legend class="col-form-label col-sm-3 pt-0">Status</legend>
                <div class="col-sm-9">
                  <div class="form-check form-check-inline">
                    <label for="status" class="form-check-label">
                    <input type="checkbox" class="form-check-input" id="status" name="status" 
                        @if( isset($model->id) ) @if( $model->status == 1 ) checked @else unchecked @endif @else checked @endif
                      />
                      Status
                    <i class="input-helper"></i></label>
                  </div>
                </div>
              </div>
            </fieldset> 
            <div class="form-group row">
              <label for="order_date" class="col-sm-3 col-form-label">Order Date</label>
              <div class="col-sm-9">
                <div id="order_date-popup" class="input-group date datepicker">
                  <input type="text" class="form-control" id="order_date" name="order_date" placeholder="dd/mm/yyyy" value="@if( isset($model->id) ) {{ Carbon\Carbon::createFromTimestamp( $model->order_date )->format('d/m/Y') }} @else {{ Carbon\Carbon::now()->format('d/m/Y') }} @endif">
                  <span class="mp-date-format">dd/mm/yyyy</span>
                  <span class="input-group-addon input-group-append border-left">
                    <span class="mdi mdi-calendar input-group-text"></span>
                  </span>
                </div>
              
              </div>
            </div>
            <div class="form-group row">
              <label for="customer" class="col-sm-3 col-form-label">Customer</label>
              <div class="col-sm-9">
                <select name="customer" class="form-control select2">
                  @foreach($customer as $s)
                  <option value="{{ $s->id }}" @if($model->m_customer_id == $s->id) selected @endif>{{ $s->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="courier" class="col-sm-3 col-form-label">Courier</label>
              <div class="col-sm-9">
                <select name="courier" class="form-control select2">
                  @foreach($courier as $s)
                  <option value="{{ $s->id }}" @if($model->m_courier_id == $s->id) selected @endif>{{ $s->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="shipping_cost" class="col-sm-3 col-form-label">Shipping Cost</label>
              <div class="col-sm-9">
                <input type="number" class="form-control {{ $errors->has('shipping_cost') ? 'is-invalid' : '' }}" id="shipping_cost" name="shipping_cost" placeholder="Shipping Cost" value="{{ $errors->has('shipping_cost') ? old('shipping_cost' ) : $model->shipping_cost }}"> 
              </div>
            </div>
            <div class="form-group row">
              <label for="weight" class="col-sm-3 col-form-label">Weight</label>
              <div class="col-sm-9">
                <input type="number" class="form-control {{ $errors->has('weight') ? 'is-invalid' : '' }}" id="weight" name="weight" placeholder="Weight" value="{{ $errors->has('weight') ? old('weight' ) : $model->weight }}"> 
              </div>
            </div>
            <div class="form-group row d-none">
              <label for="plan_shipping_date" class="col-sm-3 col-form-label">Plan Shipping Date</label>
              <div class="col-sm-9">
                <div id="plan_shipping_date-popup" class="input-group date datepicker">
                  <input type="text" class="form-control" id="plan_shipping_date" name="plan_shipping_date" placeholder="dd/mm/yyyy" value="@if( isset($model->id) ) {{ Carbon\Carbon::createFromTimestamp( $model->plan_shipping_date )->format('d/m/Y') }} @else {{ Carbon\Carbon::now()->format('d/m/Y') }} @endif">
                  <span class="mp-date-format">dd/mm/yyyy</span>
                  <span class="input-group-addon input-group-append border-left">
                    <span class="mdi mdi-calendar input-group-text"></span>
                  </span>
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label for="note" class="col-sm-3 col-form-label">Description</label>
              <div class="col-sm-9">
                <textarea name="note" id="note" class="simpleMde">{!! $model->note !!}</textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-lg-6 d-flex flex-column">
    <div class="row">
      <div class="col-md-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">  
            <h4 class="card-title mb-5">Order Status</h4>   
            <div class="form-group row">
              <label for="order_status" class="col-sm-3 col-form-label">Order Status</label>
              <div class="col-sm-9">
                <select name="order_status" class="form-control select2">
                  @foreach($orderStatus as $s)
                  <option value="{{ $s->id }}" @if($model->m_order_status_id == $s->id) selected @endif>{{ $s->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>     
            <div class="form-group row">
              <label for="actual_shipping_date" class="col-sm-3 col-form-label">Shipping Date</label>
              <div class="col-sm-9">
                <div id="actual_shipping_date-popup" class="input-group date datepicker">
                  <input type="text" class="form-control" id="actual_shipping_date" name="actual_shipping_date" placeholder="dd/mm/yyyy" value="@if( isset($model->id) ) {{ Carbon\Carbon::createFromTimestamp( $model->actual_shipping_date )->format('d/m/Y') }} @else {{ Carbon\Carbon::now()->format('d/m/Y') }} @endif">
                  <span class="mp-date-format">dd/mm/yyyy</span>
                  <span class="input-group-addon input-group-append border-left">
                    <span class="mdi mdi-calendar input-group-text"></span>
                  </span>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="tracking_number" class="col-sm-3 col-form-label">Tracking Number</label>
              <div class="col-sm-9">
                <input type="text" class="form-control {{ $errors->has('tracking_number') ? 'is-invalid' : '' }}" id="tracking_number" name="tracking_number" placeholder="Tracking Number" value="{{ $errors->has('tracking_number') ? old('tracking_number' ) : $model->tracking_number }}"> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <div class="d-flex flex-wrap justify-content-between">
                <h4 class="card-title">Skill</h4>
                <button type="button" class="btn btn-primary btn-sm btn-icon-text" id="ds_add">
                  <i class="mdi mdi-plus-box btn-icon-prepend"></i>
                  Add
                </button>
              </div>
              <div class="table-responsive-sm">
              <table class="table table-hover" id="mp_table">
                <thead>
                  <tr>
                    <th class="w-50">Menu</th>
                    <th>Quantity</th>
                    <th>Note</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <select name="menu[]" class="form-control select2e" select2_route="{{ route('menu.getdataforselect') }}">
                      </select>
                    </td>
                    <td>
                      <input type="text" class="form-control" id="quantity" name="quantity[]" placeholder="quantity" value="">
                    </td>
                    <td>
                      <input type="text" class="form-control" id="note" name="note_detail[]" placeholder="note_detail" value="">
                    </td>
                    <td>
                      <a href="#" class="text-primary mr-1 p-2" id="add_row"><i class="mdi mdi-plus"></i></a>
                    </td>
                  </tr>
                </tbody>
              </table>
              </div>
            </div>
          </div>
        </div>
        </div>
  </div>

</div>

<div class="row">
  <div class="col-lg-12 d-flex flex-column">

    <div class="card">
      <div class="card-body">   
              
        <div class="form-group float-right mb-0">        
          <a href="{{ URL::previous() }}"><button type="button" class="btn btn-lg btn-light mr-3" id="cancel" name="cancel">Cancel</button></a>     
          <button type="submit" class="btn btn-lg btn-primary">Save</button>
        </div>
      </div>
    </div>

  </div>
</div> 

</form>
@endsection

@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/select2/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/simplemde/simplemde.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
@endsection

@section('js-content')
<script type="text/javascript" src="{{ asset('/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/simplemde/simplemde.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/select.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/select2e.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/general/editor.js') }}"></script>
<script type="text/javascript">

jQuery(document).ready(function(){

  jQuery("#add_row").click(function () {
    event.preventDefault();

    var template = `
    <tr>
      <td>
        <select name="menu[]" class="form-control select2e" select2_route="{{ route('menu.getdataforselect') }}">
        
        </select>
      </td>
      <td>
        <input type="number" class="form-control" id="quantity" name="quantity[]" placeholder="quantity">
      </td>
      <td>
        <input type="text" class="form-control" id="note_detail" name="note_detail[]" placeholder="note" value="">
      </td>
      <td>
        <a href="#" class="text-danger mr-1 p-2" id="remove_row"><i class="mdi mdi-delete"></i></a>
      </td>
    </tr>
    `;
    jQuery('#mp_table > tbody:last-child').append(template);

    jQuery('select.select2e').each(function(){
      var input = jQuery(this);      
      var route = input.attr('select2_route');
      var token = jQuery("input[name='_token']").val();

      input.select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'menu',
        ajax: {
          dataType: 'json',
          type: "POST",
          url: route,
          data: function(params) {
            return {
              search: params.term,
              _token: token,
            }
          },
          processResults: function (data, page) {
            return {
              results: data
            };
          },
        }
      });

    });
  });  

  jQuery("#mp_table").on('click', '#remove_row', function () {
    jQuery(this).closest('tr').remove();
  });

  jQuery('#mpform').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) { 
      e.preventDefault();
      return false;
    }
  });

});

</script>
@endsection
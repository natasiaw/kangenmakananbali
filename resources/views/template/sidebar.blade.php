<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('home') }}"><i class="mdi mdi-shield-check menu-icon"></i> <span class="menu-title">Dashboard</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('order.index') }}"><i class="mdi mdi-file-document-box-check-outline menu-icon"></i> <span class="menu-title">Order</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('customer.index') }}"><i class="mdi mdi-account-edit menu-icon"></i> <span class="menu-title">Customer</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('customerPoint.index') }}"><i class="mdi mdi-account-edit menu-icon"></i> <span class="menu-title">Point</span></a>
      </li>
      <li class="nav-item">
        <a aria-controls="ui-basic" aria-expanded="false" class="nav-link" data-toggle="collapse" href="#sales">
            <i class="mdi mdi-account-settings menu-icon"></i> <span class="menu-title">Sales</span> <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="sales">
            <ul class="nav flex-column sub-menu">
              <li class="nav-item">
                <a class="nav-link" href="{{ route('transactions.index') }}">Transactions</a>
              </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('reportSales.index') }}">Report</a>
                </li>
            </ul>
        </div>
      </li>
      
      <li class="nav-item">
        <a aria-controls="ui-basic" aria-expanded="false" class="nav-link" data-toggle="collapse" href="#master">
            <i class="mdi mdi-account-settings menu-icon"></i> <span class="menu-title">Master</span> <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="master">
            <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('category.index') }}">Category</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('categoryType.index') }}">Category Type</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('courier.index') }}">Courier</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('menu.index') }}">Menu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('orderStatus.index') }}">Order Status</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('postalCode.index') }}">Postal Code</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('promo.index') }}">Promo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('restaurant.index') }}">Restaurant</a>
                </li>
                
            </ul>
        </div>
      </li>
         
  </ul>
</nav>
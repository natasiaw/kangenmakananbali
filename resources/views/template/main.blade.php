<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Kangen Makanan Bali</title>
    <meta name="description" content="Kangen Makanan Bali">
    <meta name="author" content="Kangen Makanan Bali">

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdn.materialdesignicons.com/4.5.95/css/materialdesignicons.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendors/base/vendor.bundle.base.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/vertical-layout-light/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/custom.css') }}">
    <link type="text/css" href="{{asset('/vendors/bootstrap-sweetalert/dist/sweetalert.css')}}" rel="stylesheet">
    <script src="{{asset('/vendors/bootstrap-sweetalert/dist/sweetalert.min.js')}}"></script>

    @yield('css-content')
    <script>
        function generateNotification($alert_type, $message) {
            var html = '<div class="alert alert-' + $alert_type + '"><button type="button" class="close" data-dismiss="alert">&times;</button>';
            html += '<p>' + $message + '</p>';
            html += '</div>';

            return $.parseHTML(html);
        }
    </script>
    @php
    $page = "";
    @endphp

</head>

<body>

    <div class="overlay">
        <img src="{{ asset('/img/loading.gif') }}" alt="loading"/>
    </div>

    <div class="container-scroller">
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-left navbar-brand-wrapper d-flex align-items-center justify-content-between">
                <a class="navbar-brand brand-logo" href="#">
                    KMB
                </a>                 
                <a class="navbar-brand brand-logo-mini" href="">
                <img src="{{ asset('/img/mp-mini.png') }}" alt="logo"/>
                </a> 
                <button class="navbar-toggler align-self-center" data-toggle="minimize" type="button">
                    <span class="mdi mdi-menu"></span>
                </button>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <ul class="navbar-nav">
                    <li class="nav-item  dropdown d-none align-items-center d-lg-flex d-none">
                        <p class="mb-0">{{ \Carbon\Carbon::now()->format('d F Y') }}</p>
                    </li>
                </ul>
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item  dropdown d-none align-items-center d-lg-flex d-none">
                        <a class="dropdown-toggle btn btn-outline-secondary btn-fw"  href="#" data-toggle="dropdown" id="pagesDropdown">                        
                            <span class="nav-profile-name"></span>         
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="pagesDropdown">
                            <div class="text-center my-3">
                                
                                <p class="dropdown-header mt-3"><strong><span class=""></span></strong></p>
                                <p class=""><span class=""></span></p>
                            </div>
                            <a class="dropdown-item" href="">
                            <i class="mdi mdi-lock-open text-primary"></i>
                            Change Password
                            </a>
                            <a class="dropdown-item" href="">
                            <i class="mdi mdi-key text-primary"></i>
                            Change PIN
                            </a>
                            <a class="dropdown-item" href="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="mdi mdi-logout text-primary"></i>
                            Logout
                            </a>
                            <form id="logout-form" action="" method="POST" style="display:none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                </button>
            </div>
        </nav>

        <div class="container-fluid page-body-wrapper">
        
        @include('template.sidebar')
        <!-- END Navbar -->

            <!-- START Main Panel -->
            <div class="main-panel">
                <!-- START Content -->
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            @yield('main-content')
                        </div>
                    </div>
                </div>
                <!-- END Content -->

                <!-- START Footer -->
                @include('template.footer')
                <!-- END Footer -->

            </div>
            <!-- END Main Panel -->
        
        </div>
    </div>

    <script type="text/javascript" src="{{ asset('/vendors/base/vendor.bundle.base.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/template.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/off-canvas.js') }}"></script>

    <!-- Custom js for this page-->
    @yield('js-content')
    <!-- End custom js for this page-->

</body>
</html>

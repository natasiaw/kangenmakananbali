@extends('template.main')
@section('main-content')
<div class="row">
  <div class="col-lg-12 d-flex flex-column grid-margin stretch-card">
   <div class="card bg-dark">
      <div class="card-body">
        <h4 class="display-4 pb-0 mb-0 text-white">Kangen Makanan Bali Overview</h4>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-md-12">
		@include('template.error')
		<div class="row">
			<div class="col-lg-12 d-flex flex-column ">
				<div class="row flex-grow">
					<div class="col-md-12 col-lg-12 grid-margin stretch-card">
							<div class="col-sm-3">
								<h2 id="next_shipping_date"></h2>
								<div class="card-title mb-3">Next Shipping Date</div>
							</div>
							<div class="col-sm-3">
								<h2 id="total_price"></h2>
								<div class="card-title mb-3">Bawa uang segini oi!!</div>
							</div>
							<div class="col-sm-3">
								<h2 id="total_shipping"></h2>
								<div class="card-title mb-3">Shipping</div>
							</div>
							<div class="col-sm-3">
								<h2 id="total_profit"></h2>
								<div class="card-title mb-3"></div>
							</div>
					</div>
				</div>
			</div>
		</div>


		<div class="row flex-grow">
			<div class="col-12 col-md-12 col-lg-6 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Orders Classification</h4>
						</div>
						<div class="table-responsive-lg">
							<table id="table-listing" class="table table-bordered table-hover">
								
							</table>
						</div>
					</div>
				</div>
			</div>

			
		</div>

		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-xl-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Orders Summary</h4>
						</div>
						<div class="table-responsive-lg">
							<table id="summarytable-listing" class="table table-bordered table-hover">
								<thead class="thead-dark">
								<tr>
									<th>&nbsp;</th> 
									<th>Customer</th>
									<th>Menu Order</th>
									<th>Note</th>
									<th>Description</th>
									<th>Status</th>
								</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	

		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-xl-12 grid-margin stretch-card">
				<div class="card">
					<div class="card-body">
						<div class="d-flex flex-wrap justify-content-between">
							<h4 class="card-title mb-3">Orders Complete Not Paid</h4>
						</div>
						<div class="table-responsive-lg">
							<table id="completenotpaidtable-listing" class="table table-bordered table-hover">
								<thead class="thead-dark">
								<tr>
									<th>&nbsp;</th> 
									<th>Customer</th>
									<th>Menu Order</th>
									<th>Bill</th>
									<th>Note</th>
									<th>Status</th>
								</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>			
@endsection
@section('css-content')
<link rel="stylesheet" type="text/css" href="{{ asset('/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
@endsection
@section('js-content')
<script type="text/javascript" src="{{ asset('/js/general/highcharts.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/general/exports-highchart.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/general/exportdata-highchart.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/datatables.net/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendors/moment/moment.min.js') }}"></script>
<script type="text/javascript">

$(document).ready(function() {
	get_ordersclassification();
	get_ordersdate();
	get_ordersummary();
	get_ordercomplatenotpaid();
	get_totalprice();

	function formatNumber(num) {
		return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
	}

	function get_ordersdate() {
		var date_route    = "{{ route('dashboard.getordersdate') }}";
		jQuery.ajax({
			url: date_route,
			dataType: "json",
			success: function(datas) {
				jQuery("#next_shipping_date").html(moment.unix( datas.actual_shipping_date ).format('DD MMM YYYY'));
			}
		});	
	}

	function get_totalprice() {
		var date_route    = "{{ route('dashboard.gettotalprice') }}";
		jQuery.ajax({
			url: date_route,
			dataType: "json",
			success: function(datas) {
				//console.log(datas);
				var totalPrice = 0;
				var totalProfit = 0;
				var totalShipping = 0;
				datas.forEach(function (item){
					item.order_detail.forEach(function (xyz){
						totalPrice += Number((xyz.original_price*xyz.quantity));
						totalProfit += Number((xyz.kmb_price*xyz.quantity)-(xyz.original_price*xyz.quantity));
					});
					totalShipping+= Number(item.shipping_cost * item.weight)+ item.additional_cost;
					totalProfit += Number(-item.additional_cost);
				});
				jQuery("#total_price").html("Rp "+formatNumber(totalPrice));
				jQuery("#total_profit").html("Rp "+formatNumber(totalProfit));
				jQuery("#total_shipping").html("Rp "+formatNumber(totalShipping));
			}
		});	
	}

	function get_ordersclassification() {
		var data_route    = "{{ route('dashboard.getordersclassification') }}";

		var groupColumn = 1;
			var t = jQuery('#table-listing').DataTable({
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			dom: 'Bfrtip',
			pageLength: 50,
			ajax: {
				type: "GET",
				url: data_route,
				dataType: "json",
				'error': function (xhr, textStatus, ThrownException) {
				console.log(xhr);
				}
			},
			columns: [
				{
					data: "id", 
					defaultContent: '',
					searchable: false,
					orderable: false,
					visible:false,
				},
				{ 
					data: "restaurant", 
					visible:false,
				},
				{ 
			data: "total",
			render: function(data, type, row) {
				return "<h4><label class='badge badge-info mr-2'>"+ data + "</label></h4>";
			
			}
			},
				{ data: "menu" },
				
			],
			"drawCallback": function ( settings ) {
				var api = this.api();
				var rows = api.rows( {page:'current'} ).nodes();
				var last=null;

				api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
				group = group;
					if ( last !== group ) {
						$(rows).eq( i ).before(
							'<tr class="group"><td colspan="10"><label class="badge badge-primary mr-2">'+group+'</label></td></tr>'
						);

						last = group;
					}
				} );
			},
			});

			t.on('draw.dt', function () {
				var info = t.page.info();
				t.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
					cell.innerHTML = i + 1 + info.start;
				});
			});
	}

	function get_ordersummary() {
		var data_route    = "{{ route('order.getdatapaid') }}";

		var groupColumn = 1;
			var t = jQuery('#summarytable-listing').DataTable({
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			dom: 'Bfrtip',
			pageLength: 50,
			ajax: {
				type: "GET",
				url: data_route,
				dataType: "json",
				'error': function (xhr, textStatus, ThrownException) {
				console.log(xhr);
				}
			},
			columns: [
				{
					data: "id", 
					defaultContent: '',
					searchable: false,
					orderable: false,
					visible:false,
				},
				{ 
          data: "customer",
          render: function(data, type, row) {
			  //console.log(row);
            var str = "<strong>"+data.name +"</strong><br>"+ data.phone +"<br>"+ data.address;
			if(data.id_m_postal_code != null){
				str += "<br>" + data.postal_code.village +", "+ data.postal_code.district +", "+ data.postal_code.regency;
				str += "<br>" + data.postal_code.province.name +", "+ data.postal_code.postal_code;
			}
            return str;
          }
        },
        { 
          data: "order_detail",
          render: function(data, type, row) {
            var str = "";
            data.forEach(function (item){
              str += item.menu.restaurant.name+" - <strong>"+item.menu.name +"</strong> : "+ item.quantity;
			  if(item.note != null){
				str += " (" + item.note +")";
			  }
			  str += "<br>";

            });
			str += row.courier.name +" - "+ row.shipping_cost +" - "+ row.weight;
            return str;
          }
        },
        { 
          data: "order_detail",
          render: function(data, type, row) {
            var str="";
            data.forEach(function (item){
              if(item.note != null){
                str += item.note +"</br>";
              }
            });
            return str;
           
          }
        },
		{ data: "note" },
        {
          data: 'order_status',
          render: function(data, type, row) {
            switch (data.id) 
            {
              case 0:
                return '<label class="badge badge-warning mr-2">'+data.name+'</label>';
              case 1:
                return '<label class="badge badge-success mr-2">'+data.name+'</label>';
              case 2:
                return '<label class="badge badge-primary mr-2">'+data.name+'</label>';
              case 3:
                return '<label class="badge badge-info mr-2">'+data.name+'</label>';
              case 4:
                return '<label class="badge badge-danger mr-2">'+data.name+'</label>';
			  default:
                return '<label class="badge badge-danger mr-2">'+data.name+'</label>';
            }
            
          }
        },
				
			]
			});

			t.on('draw.dt', function () {
				var info = t.page.info();
				t.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
					cell.innerHTML = i + 1 + info.start;
				});
			});
	}

	function get_ordercomplatenotpaid() {
		var data_route    = "{{ route('dashboard.getdatacompletenotpaid') }}";

		var groupColumn = 1;
			var t = jQuery('#completenotpaidtable-listing').DataTable({
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
			dom: 'Bfrtip',
			pageLength: 50,
			ajax: {
				type: "GET",
				url: data_route,
				dataType: "json",
				'error': function (xhr, textStatus, ThrownException) {
				console.log(xhr);
				}
			},
			columns: [
				{
					data: "id", 
					defaultContent: '',
					searchable: false,
					orderable: false,
					visible:false,
				},
				{ 
				data: "customer",
				render: function(data, type, row) {
					var str = "<strong>"+data.name +"</strong><br>"+ data.phone +"<br>"+ data.address;
					str += "<br>" + data.postal_code.village +", "+ data.postal_code.district +", "+ data.postal_code.regency;
					str += "<br>" + data.postal_code.province.name +", "+ data.postal_code.postal_code;
					return str;
				}
				},
				{ 
				data: "order_detail",
				render: function(data, type, row) {
					var str = "";
					data.forEach(function (item){
					str += item.menu.restaurant.name+" - <strong>"+item.menu.name +"</strong> : "+ item.quantity + "<br>";
					});
					return str;
				}
				},
				{ 
					data: "order_detail",
					render: function(data, type, row) {
						var total_price = 0;
						data.forEach(function (item){
						total_price += Number((item.kmb_price*item.quantity));
						});
						total_price +=(row.shipping_cost*row.weight);
					
						return "<h4>Rp "+ formatNumber(total_price) + "</h4>";
					
					}
					},
				{ data: "note" },
				{
				data: 'order_status',
				render: function(data, type, row) {
					switch (data.id) 
					{
					case 0:
						return '<label class="badge badge-warning mr-2">'+data.name+'</label>';
					case 1:
						return '<label class="badge badge-success mr-2">'+data.name+'</label>';
					case 2:
						return '<label class="badge badge-primary mr-2">'+data.name+'</label>';
					case 3:
						return '<label class="badge badge-info mr-2">'+data.name+'</label>';
					case 4:
						return '<label class="badge badge-danger mr-2">'+data.name+'</label>';
					default:
						return '<label class="badge badge-danger mr-2">'+data.name+'</label>';
					}
					
				}
				},
				
			]
			});

			t.on('draw.dt', function () {
				var info = t.page.info();
				t.column(0, { search: 'applied', order: 'applied', page: 'applied' }).nodes().each(function (cell, i) {
					cell.innerHTML = i + 1 + info.start;
				});
			});
	}
 	
  
    
});
</script>			
@endsection



<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard/getdatacompletenotpaid', ['uses' => 'HomeController@get_data_complete_not_paid', 'as' => 'dashboard.getdatacompletenotpaid']);
Route::get('/dashboard/ordersdate', ['uses' => 'HomeController@get_orders_date', 'as' => 'dashboard.getordersdate']);
Route::get('/dashboard/gettotalprice', ['uses' => 'HomeController@get_total_price', 'as' => 'dashboard.gettotalprice']);
Route::get('/dashboard/ordersclassification', ['uses' => 'HomeController@get_orders_classification', 'as' => 'dashboard.getordersclassification']);
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@index');

/* ---------- Master ---------- */

Route::get('/customer/getdata', ['uses' => 'Master\CustomerController@get_data', 'as' => 'customer.getdata']);
Route::resource('/customer', 'Master\CustomerController');

Route::get('/postalCode/getdata', ['uses' => 'Master\PostalCodeController@get_data', 'as' => 'postalCode.getdata']);
Route::post('/postalCode/getdataforselect', ['uses' => 'Master\PostalCodeController@get_data_for_select', 'as' => 'postalCode.getdataforselect']);
Route::resource('/postalCode', 'Master\PostalCodeController');

Route::get('/courier/getdata', ['uses' => 'Master\CourierController@get_data', 'as' => 'courier.getdata']);
Route::resource('/courier', 'Master\CourierController');

Route::get('/orderStatus/getdata', ['uses' => 'Master\OrderStatusController@get_data', 'as' => 'orderStatus.getdata']);
Route::resource('/orderStatus', 'Master\OrderStatusController');

Route::get('/restaurant/getdata', ['uses' => 'Master\RestaurantController@get_data', 'as' => 'restaurant.getdata']);
Route::resource('/restaurant', 'Master\RestaurantController');

Route::get('/menu/gethalal', ['uses' => 'Master\MenuController@get_halal', 'as' => 'menu.get_halal']);
Route::get('/menu/idxhalal', ['uses' => 'Master\MenuController@idx_halal', 'as' => 'menu.idx_halal']);
Route::get('/menu/getrelease', ['uses' => 'Master\MenuController@get_release', 'as' => 'menu.get_release']);
Route::get('/menu/idxrelease', ['uses' => 'Master\MenuController@idx_release', 'as' => 'menu.idx_release']);
Route::post('/menu/getdatabyid', ['uses' => 'Master\MenuController@get_data_by_id', 'as' => 'menu.getdatabyid']);
Route::post('/menu/getdataforselect', ['uses' => 'Master\MenuController@get_data_for_select', 'as' => 'menu.getdataforselect']);
Route::get('/menu/getdata', ['uses' => 'Master\MenuController@get_data', 'as' => 'menu.getdata']);
Route::resource('/menu', 'Master\MenuController');

Route::get('/categoryType/getdata', ['uses' => 'Master\CategoryTypeController@get_data', 'as' => 'categoryType.getdata']);
Route::resource('/categoryType', 'Master\CategoryTypeController');

Route::get('/category/getdata', ['uses' => 'Master\CategoryController@get_data', 'as' => 'category.getdata']);
Route::resource('/category', 'Master\CategoryController');

Route::get('/promo/getdata', ['uses' => 'Master\PromoController@get_data', 'as' => 'promo.getdata']);
Route::resource('/promo', 'Master\PromoController');



/* ---------- Order ---------- */

Route::get('/order/getdatapaid', ['uses' => 'Order\OrderController@get_data_paid', 'as' => 'order.getdatapaid']);
Route::post('/order/getinvoice', ['uses' => 'Order\OrderController@get_invoice', 'as' => 'order.getinvoice']);
Route::get('/order/getdata', ['uses' => 'Order\OrderController@get_data', 'as' => 'order.getdata']);
Route::resource('/order', 'Order\OrderController');

Route::post('/orderDetail/getdatabycustomerorderid', ['uses' => 'order\OrderDetailController@get_data_by_customer_order_id', 'as' => 'orderDetail.getdatabycustomerorder']);
Route::resource('/orderDetail', 'Order\OrderDetailController');

Route::get('/customerPoint/getdata', ['uses' => 'order\CustomerPointController@get_data', 'as' => 'customerPoint.getdata']);
Route::resource('/customerPoint', 'Order\CustomerPointController');

/* ---------- Sales ---------- */

Route::get('/transactions/getdata', ['uses' => 'Finance\TransactionsController@get_data', 'as' => 'transactions.getdata']);
Route::resource('/transactions', 'Finance\TransactionsController');


Route::get('/reportSales/getnetpermonth', ['uses' => 'Sales\ReportController@get_net_per_month', 'as' => 'reportSales.getnetpermonth']);
Route::get('/reportSales/getgrosspermonth', ['uses' => 'Sales\ReportController@get_gross_per_month', 'as' => 'reportSales.getgrosspermonth']);
Route::get('/reportSales/getadspermonth', ['uses' => 'Sales\ReportController@get_ads_per_month', 'as' => 'reportSales.getadspermonth']);
Route::get('/reportSales/getexpensespermonth', ['uses' => 'Sales\ReportController@get_expenses_per_month', 'as' => 'reportSales.getexpensespermonth']);
Route::get('/reportSales/getcustomerpermonth', ['uses' => 'Sales\ReportController@get_customer_per_month', 'as' => 'reportSales.getcustomerpermonth']);
Route::get('/reportSales/getprofitpermonth', ['uses' => 'Sales\ReportController@get_profit_per_month', 'as' => 'reportSales.getprofitpermonth']);
Route::post('/reportSales/getreport', ['uses' => 'Sales\ReportController@get_report', 'as' => 'reportSales.getreport']);
Route::post('/reportSales/getcustomer', ['uses' => 'Sales\ReportController@get_total_customer', 'as' => 'reportSales.getcustomer']);
Route::get('/reportSales/getdata', ['uses' => 'Sales\ReportController@get_data', 'as' => 'reportSales.getdata']);
Route::resource('/reportSales', 'Sales\ReportController');